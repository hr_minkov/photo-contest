use photo_contest;

lock tables `categories` write;
alter table `categories`
    disable keys;
insert into `categories`
values (1, 'People'),
       (2, 'Architecture'),
       (3, 'Interior'),
       (4, 'Animals'),
       (5, 'Food & drink'),
       (6, 'Nature'),
       (7, 'Others')
;
alter table `categories`
    enable keys;
unlock tables;

lock tables `roles` write;
alter table `roles`
    disable keys;
insert into roles
values (1, 'junkie'),
       (2, 'organizer')
;
alter table `roles`
    enable keys;
unlock tables;


lock tables `users_details` write;
alter table `users_details`
    disable keys;
insert into `users_details`
values (1, 'Luboslav', 'Gergov', -1, 2, 1, 'https://i.imgur.com/1IZ5380.png'),
       (2, 'Hristo', 'Minkov', -1, 2, 1, 'https://imgur.com/7CAggWw.png'),
       (3, 'Vidyo', 'Tanev', 73, 1, 1, 'https://imgur.com/Sg38Wna.png'),
       (4, 'Grigori', 'Alexandrov', 648, 1, 1, 'https://imgur.com/KgAQLK8.png'),
       (5, 'Bogomil', 'Venev', 42, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (6, 'Kliment', 'Lukanov', 69, 1, 1, 'https://imgur.com/zbQpVJx.png'),
       (7, 'Zheko', 'Dimitrov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (8, 'Razvigor', 'Tsvetanov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (9, 'Gero', 'Karapetrov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (10, 'Boyko', 'Pironev', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (11, 'Rodopa', 'Pencheva', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (12, 'Kosena', 'Pingova', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (13, 'Bisera', 'Vasileva', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (14, 'Marina', 'Vaseva', 1243, 1, 1, 'https://imgur.com/jB9CuhU.png'),
       (15, 'Vasilka', 'Boteva', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (16, 'Vrabka', 'Todorova', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (17, 'Todora', 'Kirilova', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (18, 'Gizda', 'Ruseva', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (19, 'Iva', 'Tsankova', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (20, 'Marko', 'Markov', 55, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (21, 'Petar', 'Despov', 312, 1, 1, 'https://imgur.com/JTpxGBf.png'),
       (22, 'Sasho', 'Marangozov', 5, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (23, 'Maria', 'Nikolova', 465, 1, 1, 'https://imgur.com/j1oBwTc.png'),
       (24, 'Boryana', 'Milenova', 6, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (25, 'Marin', 'Gergov', 45, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (26, 'Ivan', 'Velev', 18, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (27, 'Grisha', 'Georgiev', 46, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (28, 'Kosta', 'Kostov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (29, 'Martin', 'Petkov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (30, 'Danislav', 'Veselinov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (31, 'Preslav', 'Dimitrov', 655, 1, 1, 'https://imgur.com/ySlQ8jG.png'),
       (32, 'Trifon', 'Trifonov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (33, 'Dimitar', 'Kostadinov', 0, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (34, 'Borimira', 'Hadjieva', 123, 1, 1, 'https://imgur.com/UizXWQw.png'),
       (35, 'Bisera', 'Ivanova', 321, 1, 1, 'https://imgur.com/5vkzDpa.png'),
       (36, 'Nona', 'Koleva', 52, 1, 1, 'https://imgur.com/kPYrZCM.png'),
       (37, 'Nikol', 'Georgieva', 0, 1, 0, 'https://imgur.com/kPYrZCM.png'),
       (38, 'Nikoleta', 'Borisova', 0, 1, 0, 'https://imgur.com/kPYrZCM.png'),
       (39, 'Stefan', 'Genchev', 0, 1, 0, 'https://imgur.com/kPYrZCM.png'),
       (40, 'Bobrislav', 'Alexiev', 0, 1, 0, 'https://imgur.com/kPYrZCM.png');
alter table `users_details`
    enable keys;
unlock tables;

lock tables `users_credentials` write;
alter table `users_credentials`
    disable keys;
insert into `users_credentials`
values (1, 'luboslav', 'gergov'),
       (2, 'hristo', 'minkov'),
       (3, 'vidyo', 'tanev'),
       (4, 'grigori', 'alexandrov'),
       (5, 'bogomil', 'venev'),
       (6, 'kliment', 'lukanov'),
       (7, 'zheko', 'dimitrov'),
       (8, 'razvigor', 'tsvetanov'),
       (9, 'gero', 'karapetrov'),
       (10, 'boyko', 'pironev'),
       (11, 'rodopa', 'pencheva'),
       (12, 'kosena', 'pingova'),
       (13, 'sara', 'vasileva'),
       (14, 'marina', 'vaseva'),
       (15, 'vasilka', 'boteva'),
       (16, 'vrabka', 'todorova'),
       (17, 'todora', 'kirilova'),
       (18, 'gizda', 'ruseva'),
       (19, 'iva', 'tsankova'),
       (20, 'marko', 'markov'),
       (21, 'petar', 'despov'),
       (22, 'sasho', 'marangozov'),
       (23, 'maria', 'nikolova'),
       (24, 'boryana', 'milenova'),
       (25, 'marin', 'gergov'),
       (26, 'ivan', 'velev'),
       (27, 'grisha', 'georgiev'),
       (28, 'kosta', 'kostov'),
       (29, 'martin', 'petkov'),
       (30, 'danislav', 'veselinov'),
       (31, 'preslav', 'dimitrov'),
       (32, 'trifon', 'trifonov'),
       (33, 'dimitar', 'kostadinov'),
       (34, 'borimira', 'hadjieva'),
       (35, 'bisera', 'ivanova'),
       (36, 'nona', 'koleva'),
       (37, 'nikol', 'georgieva'),
       (38, 'nikoleta', 'borisova'),
       (39, 'stefan', 'genchev'),
       (40, 'bobrislav', 'alexiev');
alter table `users_credentials`
    enable keys;
unlock tables;


lock tables `contests` write;
alter table `contests`
    disable keys;
insert into `contests`
values (1, 'Finished open contest', '2021-03-15T00:00:00', '2021-03-22T00:00:00', '2021-03-22T23:00:00', 1, 1,
        'https://imgur.com/IuvRA2e.png'),
       (2, 'Phase 1 open contest', '2021-04-01T00:00:00', '2021-04-25T00:00:00', '2021-04-25T20:00:00', 2, 1,
        'https://imgur.com/e3oRL8x.png'),
       (3, 'Phase 2 private contest', '2021-03-25T00:00:00', '2021-04-20T00:00:00', '2021-04-20T20:00:00', 3, 0,
        'https://imgur.com/AMEYawN.png'),
       (4, 'Not started open contest', '2021-04-21T00:00:00', '2021-04-27T00:00:00', '2021-04-27T22:00:00', 4, 1,
        'https://imgur.com/cvZ0e0m.png'),
       (5, 'Food & Drink', '2021-04-07T00:00:00', '2021-04-27T00:00:00', '2021-04-27T20:00:00', 5, 1,
        'https://imgur.com/EFzzavJ.png'),
       (6, 'Narute', '2021-03-19T00:00:00', '2021-04-10T00:00:00', '2021-04-30T20:00:00', 6, 1,
        'https://imgur.com/iG1DyIO.png'),
       (7, 'Finished architecture', '2021-03-15T00:00:00', '2021-04-10T00:00:00', '2021-04-10T23:00:00', 2, 1,
        'https://imgur.com/DpKedPZ.png'),
       (8, 'Finished open animals', '2021-03-10T00:00:00', '2021-04-5T00:00:00', '2021-04-05T23:00:00', 4, 1,
        'https://imgur.com/kadKFzR.png'),
       (9, 'Finished open people', '2021-03-05T00:00:00', '2021-04-01T00:00:00', '2021-04-01T23:00:00', 1, 1,
        'https://imgur.com/OBVAyQ4.png');
alter table `contests`
    enable keys;
unlock tables;

lock tables `contests_jurors` write;
alter table `contests_jurors`
    disable keys;
insert into `contests_jurors`
values (1, 1, 1),
       (2, 1, 2),
       (3, 2, 1),
       (4, 2, 2),
       (5, 3, 1),
       (6, 3, 2),
       (7, 3, 14),
       (8, 4, 1),
       (9, 4, 2),
       (10, 5, 1),
       (11, 5, 2),
       (12, 5, 14),
       (13, 7, 1),
       (14, 7, 2),
       (15, 7, 14),
       (16, 7, 31),
       (17, 8, 1),
       (18, 8, 2),
       (19, 8, 14),
       (20, 8, 31),
       (21, 9, 1),
       (22, 9, 2),
       (23, 9, 14),
       (24, 9, 31),
       (25, 6, 31),
       (26, 6, 1),
       (27, 6, 2)
;
alter table `contests_jurors`
    enable keys;
unlock tables;

lock tables `contests_participants` write;
alter table `contests_participants`
    disable keys;
insert into `contests_participants`
values (1, 1, 20),
       (2, 1, 3),
       (3, 1, 4),
       (4, 1, 5),
       (5, 1, 6),
       (8, 2, 7),
       (9, 2, 8),
       (10, 2, 9),
       (11, 2, 10),
       (12, 2, 11),
       (15, 3, 12),
       (16, 3, 13),
       (17, 3, 15),
       (18, 3, 16),
       (19, 3, 17),
       (22, 5, 4),
       (23, 5, 5),
       (24, 5, 6),
#        (25, 5, 7),
#        (26, 5, 8),
       (29, 6, 9),
       (30, 6, 10),
       (31, 6, 11),
       (32, 6, 12),
       (33, 6, 13),
       (36, 7, 26),
       (37, 7, 27),
       (38, 7, 28),
       (39, 7, 29),
       (40, 7, 30),
       (41, 8, 31),
       (42, 8, 32),
       (43, 8, 33),
       (44, 8, 34),
       (45, 8, 35),
       (46, 9, 21),
       (47, 9, 22),
       (48, 9, 23),
       (49, 9, 24),
       (50, 9, 25);
alter table `contests_participants`
    enable keys;
unlock tables;

lock tables `photos` write;
alter table `photos`
    disable keys;
insert into `photos`
values (1, 'Vietnam Woman', 'Woman in blue and white floral dress wearing black hat standing near brown wooden door.',
        20, 1, '.jpg'),
       (2, 'Old man resting', 'Street capture from walks on Barangaroo boardwalk.', 3, 1, '.jpg'),
       (3, 'Photographer', 'Boy in black and gray jacket sitting on brown grass during daytime.', 4, 1, '.jpg'),
       (4, 'Happy Family', 'This family is its own kind of beautiful.', 5, 1, '.jpg'),
       (5, 'Man smoking', 'Man in gray jacket smoking.', 6, 1, '.jpg'),
       (6, 'NY Train Station', 'White and black train station.', 7, 2, '.jpg'),
       (7, 'White and Blue', 'White concrete building under blue sky during daytime.', 8, 2, '.jpg'),
       (8, 'Delmonico in NYC',
        'The restaurant has remained closed since the pandemic-related shutdown on dining in March.', 9, 2, '.jpg'),
       (9, 'White Sky scraper', 'High-rise building in Chicago.', 10, 2, '.jpg'),
       (10, 'NYC Sky scraper', 'White concrete building under white sky during daytime', 11, 2, '.jpg'),
       (11, 'Sacred space', 'Green potted plants and our home meditation room.', 12, 3, '.jpg'),
       (12, 'Carpets', 'Our Beautiful restroom with carpets on the floor.', 13, 3, '.jpg'),
       (13, 'Dining table',
        'Industrial and modern interior in Cracow, Poland. Two chairs and table. Lamp and modern poster in frame.',
        15, 3, '.jpg'),
       (14, 'Gaming Room', 'My sacred place, where I am chilling after 9 to 5 workday.', 16, 3, '.jpg'),
       (15, 'Japanese Room', 'Black framed glass and sliding door. Japanese interior design.', 17, 3, '.jpg'),
#        (16, 'Tiger Zoo', 'Tiger lying on brown soil. Walter Zoo, Gossau, Schweiz.', 15, 4, '.jpg'),
#        (17, 'Black monkey', 'Black monkey on brown wooden stick. Walter Zoo, Gossau, Schweiz.', 18, 4, '.jpg'),
#        (18, 'Birds Love', 'Green and yellow bird on brown tree branch photo. Free and wild.', 19, 4, '.jpg'),
#        (19, 'Old dog', 'White and brown long coated dog on brown wooden dock during daytime.', 20, 4, '.jpg'),
#        (20, 'Mother Love', 'Selective photography of gray monkey with her child.', 3, 4, '.jpg'),
       (21, 'Just Pizza', 'Pizza with green leaves on brown wooden round plate.', 4, 5, '.jpg'),
       (22, 'Biscuit Ice cream', 'Pink and white biscuits ice cream in a crispy funnel.', 5, 5, '.jpg'),
       (23, 'My Breakfast', 'My routine daily breakfast on brown wooden chopping board with white ceramic bowl.',
        6, 5, '.jpg'),
#        (24, 'Just Sushi',
#         'Sushi plate with chopsticks and soya souce homemade. This is my first try and I think it is very successful',
#         7, 5, '.jpg'),
#        (25, 'Colorful dessert', 'Colorful dessert from a neighborhood store in the center of Sofia.', 8, 5, '.jpg'),
       (26, 'Erupting volcano', 'The erupting volcano in Iceland.', 9, 6, '.jpg'),
       (27, 'Beautiful mountain', 'Green and brown mountain under white sky during daytime.', 10, 6, '.jpg'),
       (28, 'Black Stone', 'Clear and black stone photo.', 11, 6, '.jpg'),
       (29, 'Blue Sky', 'Person in black jacket standing on gray rock formation under blue sky during daytime.', 12, 6,
        '.jpg'),
       (30, 'Cherry Blossom', 'White cherry blossom tree during daytime.', 13, 6, '.jpg'),
       (31, 'Vietnamese lady',
        'Vietnamese lady with blue and and gray floral top and purple hat on their national day of culture.', 21, 9,
        '.jpg'),
       (32, 'Woman Sunset', 'Woman holding white hat while standing on yellow flower field.', 22, 9, '.jpg'),
       (33, 'Untypical old lady', 'Woman wearing brown cardigan with flower on head.', 23, 9, '.jpg'),
       (34, 'Hejab', 'Wearing hejab in Yazd bazaar', 24, 9, '.jpg'),
       (35, 'Happy life', 'The moments of the life when you feel the most happy', 25, 9, '.jpg'),
       (36, 'Stair lines', 'Man standing on stair at the newest mall building in Bulgaria', 26, 7, '.jpg'),
       (37, 'White Building', 'The white and the symmetric windows.', 27, 7, '.jpg'),
       (38, 'Russian Science', 'Russian Academy of Science ceiling with light fixture.', 28, 7, '.jpg'),
       (39, 'Las Vegas', 'The newest sky scraper at Las Vegas, Nevada, The Strip during cloudy day.', 29, 7, '.jpg'),
       (40, 'Post Office',
        'Main Post Office Skopje crazy-looking central post office is located 75m northwest of Ploštad Makedonija (Macedonia Square).',
        30, 7, '.jpg'),
       (41, 'Bald eagle', 'A portrait of the rare Bald eagle specie.', 31, 8, '.jpg'),
       (42, 'Sea lion', 'Sea lion posing for the cameraman at his natural habitat.', 32, 8, '.jpg'),
       (43, 'Wide flamingos', 'Flock of flamingos on gray dirt road during daytime.', 33, 8, '.jpg'),
       (44, 'Squirrel eating', 'Squirrel eating a nut.', 34, 8, '.jpg'),
       (45, 'Hunting', 'Brown short coated dog on green grass field during daytime.', 35, 8, '.jpg');
alter table `photos`
    enable keys;
unlock tables;

lock tables `reviews` write;
alter table `reviews`
    disable keys;
insert into `reviews`
values (1, 10, 'Fantastic photo! Good job.', 1),
       (2, 8, 'Very nice!', 2),
       (3, 4, 'Like the colors but can be better', 1),
       (4, 7, 'Cool photo, gz!', 2),
       (5, 6, 'Nice job', 1),
       (6, 6, 'Great perspective', 2),
       (7, 2, 'Try harder next time', 1),
       (8, 1, 'Are u kidding?!', 2),
       (9, 8, 'Incredible!', 1),
       (10, 9, 'This is some good use of colors and light', 2),
       (11, 10, 'Fantastic photo! Good job.', 1),
       (12, 8, 'Very nice!', 2),
       (13, 4, 'Like the colors but can be better', 14),
       (14, 7, 'Cool photo, gz!', 1),
       (15, 6, 'Nice job', 2),
       (16, 6, 'Great perspective', 14),
       (17, 2, 'Try harder next time', 1),
       (18, 1, 'Are u kidding?!', 2),
       (19, 8, 'Incredible!', 14),
       (20, 9, 'This is some good use of colors and light', 1),
       (21, 9, 'I like you style', 2),
       (22, 3, 'Not too good', 14),
       (23, 2, 'Well, you must e a really nice person', 1),
       (24, 1, 'The most important thing is the we are all alive and healthy', 2),
       (25, 5, 'Decent work, but can be better', 14),
       (26, 4, 'Fantastic photo! Good job.', 1),
       (27, 7, 'Very nice!', 2),
       (28, 5, 'Like the colors but can be better', 14),
       (29, 5, 'Cool photo, gz!', 1),
       (30, 8, 'Nice job', 2),
       (31, 7, 'Great perspective', 14),
       (32, 3, 'Try harder next time', 1),
       (33, 2, 'Are u kidding?!', 2),
       (34, 9, 'Incredible!', 14),
       (35, 8, 'This is some good use of colors and light', 1),
       (36, 8, 'I like you style', 2),
       (37, 3, 'Not too good', 14),
       (38, 1, 'Well, you must e a really nice person', 1),
       (39, 2, 'The most important thing is the we are all alive and healthy', 2),
       (40, 4, 'Decent work, but can be better', 14),
       (41, 9, 'Fantastic photo! Good job.', 1),
       (42, 7, 'Very nice!', 2),
       (43, 5, 'Like the colors but can be better', 1),
       (44, 4, 'Cool photo, gz!', 2),
       (45, 5, 'Nice job', 1),
       (46, 5, 'Great perspective', 2),
       (47, 3, 'Try harder next time', 1),
       (48, 3, 'Are u kidding?!', 2),
       (49, 9, 'Incredible!', 1),
       (50, 7, 'This is some good use of colors and light', 2),
       (51, 19, 'Fantastic photo! Good job.', 1),
       (52, 7, 'Very nice!', 2),
       (53, 4, 'Like the colors but can be better', 14),
       (54, 5, 'Cool photo, gz!', 1),
       (55, 5, 'Nice job', 2)
;
alter table `reviews`
    enable keys;
unlock tables;

lock tables `photos_reviews` write;
alter table `photos_reviews`
    disable keys;
insert into `photos_reviews`
values (1, 1, 1),
       (2, 1, 2),
       (3, 2, 3),
       (4, 2, 4),
       (5, 3, 5),
       (6, 3, 6),
       (7, 4, 7),
       (8, 4, 8),
       (9, 5, 9),
       (10, 5, 10),
       (11, 31, 11),
       (12, 31, 12),
       (13, 31, 13),
       (14, 32, 14),
       (15, 32, 15),
       (16, 32, 16),
       (17, 33, 17),
       (18, 33, 18),
       (19, 33, 19),
       (20, 34, 20),
       (21, 34, 21),
       (22, 34, 22),
       (23, 35, 23),
       (24, 35, 24),
       (25, 35, 25),
       (26, 36, 26),
       (27, 36, 27),
       (28, 36, 28),
       (29, 37, 29),
       (30, 37, 30),
       (31, 37, 31),
       (32, 38, 32),
       (33, 38, 33),
       (34, 38, 34),
       (35, 39, 35),
       (36, 39, 36),
       (37, 39, 37),
       (38, 40, 38),
       (39, 40, 39),
       (40, 40, 40),
       (41, 41, 41),
       (42, 41, 42),
       (43, 41, 43),
       (44, 42, 44),
       (45, 42, 45),
       (46, 42, 46),
       (47, 43, 47),
       (48, 43, 48),
       (49, 43, 49),
       (50, 44, 50),
       (51, 44, 51),
       (52, 44, 52),
       (53, 45, 53),
       (54, 45, 54),
       (55, 45, 55)
;
alter table `photos_reviews`
    enable keys;
unlock tables;

lock tables `contests_winners` write;
alter table `contests_winners`
    disable keys;
insert into `contests_winners`
values (1, 1, 3, 1, 9.6),
       (2, 1, 4, 2, 4.1),
       (3, 1, 5, 2, 4.1),
       (5, 1, 1, 3, 3.8),
       (6, 7, 40, 1, 9.1),
       (7, 7, 36, 2, 7.6),
       (8, 7, 28, 3, 4.6),
       (9, 8, 41, 1, 8.9),
       (10, 8, 44, 2, 7.3),
       (11, 8, 45, 3, 7.0),
       (12, 9, 35, 1, 7.8),
       (13, 9, 31, 2, 7.5),
       (14, 9, 32, 3, 7.2)
;
alter table `contests_winners`
    enable keys;
unlock tables;

