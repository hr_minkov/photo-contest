use photo_contest;

create or replace table categories
(
    category_id int auto_increment
        primary key,
    name        varchar(20) not null,
    constraint categories_name_uindex
        unique (name)
);

create or replace table contests
(
    contest_id  int auto_increment
        primary key,
    title       varchar(50) not null,
    start_ph1   datetime    not null,
    start_ph2   datetime    not null,
    end_ph2     datetime    not null,
    category_id int         not null,
    is_open     tinyint(1)  not null,
    cover_link    varchar(500) ,
    constraint contests_title_uindex
        unique (title),
    constraint contests_categories_fk
        foreign key (category_id) references categories (category_id)
);

create or replace table roles
(
    role_id int auto_increment
        primary key,
    name    varchar(20) not null,
    constraint roles_name_uindex
        unique (name)
);

create or replace table users_credentials
(
    user_id  int auto_increment
        primary key,
    username varchar(20) not null,
    password varchar(20) not null,
    constraint users_credentials_username_uindex
        unique (username)
);

create or replace table users_details
(
    user_id        int auto_increment
        primary key,
    first_name     varchar(20)          not null,
    last_name      varchar(20)          not null,
    ranking_points int        default 0 not null,
    role_id        int                  not null,
    active         tinyint(1) default 1 not null,
    avatar_link      varchar(500)          ,
    constraint users_details_roles_fk
        foreign key (role_id) references roles (role_id)
);

create or replace table contests_jurors
(
    id         int auto_increment
        primary key,
    contest_id int not null,
    juror_id   int not null,
    constraint contests_jurors_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contests_jurors_users_details_fk
        foreign key (juror_id) references users_details (user_id)
);

create or replace table contests_participants
(
    id         int auto_increment
        primary key,
    contest_id int not null,
    user_id    int not null,
    constraint contests_participants_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contests_participants_users_details_fk
        foreign key (user_id) references users_details (user_id)
);

create or replace table photos
(
    photo_id   int auto_increment
        primary key,
    title      varchar(50) not null,
    story      text        not null,
    user_id    int         not null,
    contest_id int         not null,
    type       varchar(20) not null,
    constraint photos_title_uindex
        unique (title),
    constraint photos_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint photos_user_details_fk
        foreign key (user_id) references users_details (user_id)
);

create or replace table contests_winners
(
    id int auto_increment
        primary key,
    contest_id int    not null,
    photo_id   int    not null,
    place      int    not null,
    points     double not null,
    constraint contests_winners_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contests_winners_photos_fk
        foreign key (photo_id) references photos (photo_id)
);

create or replace table reviews
(
    review_id int auto_increment
        primary key,
    score     int  default 3              not null,
    comment   text default 'not reviewed' not null,
    juror_id  int                         not null,
    constraint reviews_users_details_fk
        foreign key (juror_id) references users_details (user_id)
);

create or replace table photos_reviews
(
    id int auto_increment
        primary key,
    photo_id  int not null,
    review_id int not null,
    constraint photos_reviews_photos_fk
        foreign key (photo_id) references photos (photo_id),
    constraint photos_reviews_reviews_fk
        foreign key (review_id) references reviews (review_id)
);

