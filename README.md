## **Photo Contest** 
### `by Luboslav Gergov & Hristo Minkov`
#### Photo Contest is a application that can allows you to easily manage online photo contests, designed and implemented by Lyubo & Hristo following the specifications of the Telerik Academy Alpha Java A25 Final Team Project assignement. Currently the application supports а REST API whose details you can find in the link below. Soon it will have a GUI too hopefully! :) 
---

### [**Trello Board**](https://trello.com/b/93nPdGD0/photo-contest "Photo Contest Trello Board")  

### [**Swagger Doc**](http://localhost:8080/swagger-ui.html "Photo Contest swagger doc")
---  
- ## Project initialization
    `In this section we will discus how to set up the project on Microsoft Windows 10 environment using Git Bash 2.29.2.2, IntelliJ IDEA Ultimate 2020.3.1 and MariaDB Server 10.5.9 but of course you can use any tool or platform you like. Download links are available in the next section.`  

    `From here on out we will assume that you have downloaded and installed Git Bash & IntelliJ IDEA successfully or are familiar enough with their equivalent functionalities for the tool of your choosing.`
    
  - ### Project source code & tools
    Project source code is available at this GitLab [**`link`**](https://gitlab.com/luboslavgergov/photo-contest.git "GitLab link")  
    Download GitBash [**`here`**](https://git-scm.com/downloads "GitBash download")  
    Download IntelliJ IDEA [**`here`**](https://www.jetbrains.com/idea/download/#section=windows "IntelliJ IDEA download")  

  - ### Downloading the project from GitLab
   
    - Navigate to the folder where you want to copy the project on you file system, right-click and choose `"Git Bash here"` from the context menu:    
  
        ![alt text](https://imgur.com/LuT4suP.png "Run Git Bash")  

    - Right now you should be seeing something like this console:  

        ![alt text](https://imgur.com/Q73frIp.png "Run Git Bash")  

    - It's time to get back to [**`GitLab`**](https://gitlab.com/luboslavgergov/photo-contest "GitLab link"). Hit `"Clone"` and then copy the highlighted link (note that it is important to select the HTTPS option):  
  
         ![alt text](https://imgur.com/ftpcsZl.png "Copy repo link")  

    - Return to Git Bash and type in `"git clone"`, then paste the link that you just copied, then hit `[Enter]`. In a few moments all required files should be downloaded automatically to the specified directory on your file system. The message should look like this:  
  
         ![alt text](https://imgur.com/MhyblQH.jpg "Run GitBash")  

     - You are now ready to run the project with your favorite Java IDE. In the following example we will use IntelliJ IDEA.  
  
   - ## Opening the project in IntelliJ IDEA Ultimate

      - What you have to do next is start IntelliJ IDEA and choose `Open` from the welcome screen menu:  
  
        ![alt text](https://imgur.com/fwACBUJ.png "Open project")   

      - Navigate to the folder where you just downloaded the project files and select the subfolder named `PhotoContest` (Disclaimer: name can be slightly different for you but it is very important that you select the folder with the small black square in the lower right-hand corner)
      
        ![alt text](https://imgur.com/6IG6wUx.jpg  "Import")  

      - After you click `OK` please allow a few moments needed to build the project. If you were patient enough you should be seeing something like this right now:

        ![alt text](https://imgur.com/GfhlsZa.jpg  "Import")  

        Please note that the `java` folders in packages `src` and `tests` should be colored in blue and green respectively. If, for some reason they are not, be adviced that it is probably best to start over.  

        The next step is to setup the project database. We will discuss this shortly.  

   - ## Setting up the project database server

      - Find and click the `"Database"` menu which should be in the upper right-hand corner of the window (you can also find it through VIew->Tool windows->Database, as shown in case you are using a different layout)  
  
        ![alt text](https://imgur.com/cjwCOju.jpg "Open databse menu")   

      - Next hit he small `"+"` sign and choose Data source->MariaDB.
      
        ![alt text](https://imgur.com/ALlMs6l.png "MariaDB setup")  

      - You should see this window now. You will need to register a User and Password. The project uses photocontest/photocontest by default. You can also test your connection, just to make sure:

        ![alt text](https://imgur.com/qp0wJV1.jpg "Register")  

      - You can use other DB credentials if you want to. All you have to do is set them up in scr->main->resources->application.properties, as shown below:  
  
        ![alt text](https://imgur.com/FHDbhqy.jpg "Open databse menu")   

      - You will be creating a new schema shortly. In order to do that right-click `"Schemas"` and select New->Schema.
      
        ![alt text](https://imgur.com/RGbgkFI.png "Create Schema")  

      - Choose `"photo_contest"` as Name and hit `"Ok"`:

        ![alt text](https://imgur.com/QYfRNM3.jpg "Create Schema")  


      - Navigate to the `"DB"` package next and open the `"create_db.sql"` file. Then right-click anywhere inside it and select `"Execute->default"`:   
        
        ![alt text](https://imgur.com/if8J9br.jpg "DB create")

      - Make sure you have selected the whole script option before you click:
      
        ![alt text](https://imgur.com/NrR7Mkx.jpg "DB create")  

      - If we haven`t screwed up something, your database should now be created. You can check it out by toggling the ticks:

        ![alt text](https://imgur.com/u0s9Bgy.jpg "DB create")  

      - Now that we have a db schema we need to fill it up with some mock data. We will do that uding the other script file - `"fill_db.sql"`. Execute the script the same way you did the previous:
      
        ![alt text](https://imgur.com/DevQeQT.jpg "DB fill")  

      - If you got a bunch of green-colored message like this one all is fine and you can now run the project. If not - you will have to start over again (don`t mention our mammas please!):

        ![alt text](https://imgur.com/M1OLUqA.jpg "DB fill")  

      - You are now all set and you can finally run the app by clicking the green triangle. Make sure `"PhotoContestApplication"` is selected as run configuration:

        ![alt text](https://imgur.com/C6cHiP0.jpg "Run project")  

        Last but not least, here`s a diagram that you might find useful... or not: 

   - ## Database Realtions

      ![alt text](https://imgur.com/OoLJxTP.jpg "DB Relations")  

