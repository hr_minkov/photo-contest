package finalproject.photocontest.utils;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.PhotoStorageException;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.PhotoResourceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class FileSystemPhotoResourceHandlerTests {
    @Mock
    PhotoResourceRepository mockPhotoFileRepository;

    @Mock
    PhotoObjectRepository mockPhotoObjectRepository;


    @Mock
    MockMultipartFile mockMultipartFile;

    @InjectMocks
    FileSystemPhotoResourceHandler fileSystemPhotoResourceHandler;


    private static Photo photo;
    private static MultipartFile file;


    @BeforeEach
    private void setup() {
        photo = Helpers.createPhoto();

    }


    @Test
    public void persistMultipartPhoto_Should_StorePhoto() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.create(photo))
                .thenReturn(null);

        //Act
        fileSystemPhotoResourceHandler.persistMultipartPhoto(photo, mockMultipartFile);

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .create(photo);
        Mockito.verify(mockPhotoFileRepository, Mockito.times(1))
                .store(mockMultipartFile, photo.getAssociatedFileName());
    }

    @Test
    public void persistMultipartPhoto_Should_Throw__When_StorePhotoFails() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.create(photo))
                .thenReturn(null);
        Mockito.doThrow(new PhotoStorageException()).when(mockPhotoFileRepository).store(mockMultipartFile, photo.getAssociatedFileName());

        //Act, Assert
        Assertions.assertThrows(PhotoStorageException.class,
                () -> fileSystemPhotoResourceHandler.persistMultipartPhoto(photo, mockMultipartFile));
    }

    @Test
    public void deleteMultipartPhotoIfPresent_Should_DeletePhoto() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getByUserAndContestId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(photo);
        Mockito.when(mockPhotoObjectRepository.delete(photo))
                .thenReturn(null);
        Mockito.doNothing().when(mockPhotoFileRepository).deleteByName(photo.getAssociatedFileName());

        //Act
        fileSystemPhotoResourceHandler.deleteMultipartPhotoIfPresent(Mockito.anyInt(), Mockito.anyInt());

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getByUserAndContestId(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .delete(photo);
        Mockito.verify(mockPhotoFileRepository, Mockito.times(1))
                .deleteByName(photo.getAssociatedFileName());
    }

    @Test
    public void deleteMultipartPhotoIfPresent_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getByUserAndContestId(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());


        //Act
        fileSystemPhotoResourceHandler.deleteMultipartPhotoIfPresent(Mockito.anyInt(), Mockito.anyInt());

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getByUserAndContestId(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void deleteMultipartPhotoIfPresent_Should_Throw__When_DeletePhotoFails() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getByUserAndContestId(Mockito.anyInt(),Mockito.anyInt()))
                .thenReturn(photo);
        Mockito.doThrow(new PhotoStorageException()).when(mockPhotoFileRepository).deleteByName(photo.getAssociatedFileName());

        //Act, Assert
        Assertions.assertThrows(PhotoStorageException.class,
                () -> fileSystemPhotoResourceHandler.deleteMultipartPhotoIfPresent(Mockito.anyInt(),Mockito.anyInt()));
    }
}
