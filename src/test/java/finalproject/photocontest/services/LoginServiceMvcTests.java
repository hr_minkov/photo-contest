package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.AuthenticationException;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.User;
import finalproject.photocontest.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpSession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class LoginServiceMvcTests {

    @Mock
    UserService mockUserService;

    @Mock
    MockHttpSession mockHttpSession;

    @InjectMocks
    LoginServiceMvcImpl loginServiceMvc;

    private User junkie;
    private User organizer;

    @BeforeEach
    public void setup() {
        junkie = Helpers.createJunkie();
        organizer = Helpers.createOrganizer();
    }

    @Test
    public void authenticateUser_Should_NotThrow_When_CredentialsValid() {

        // Arrange
        Mockito.when(mockUserService.getByCredentials(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(junkie);

        // Act
        loginServiceMvc.authenticateUser(Mockito.anyString(), Mockito.anyString());

        // Assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .getByCredentials(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void authenticateUser_Should_Throw_When_CredentialsNotValid() {

        // Arrange
        Mockito.when(mockUserService.getByCredentials(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());

        Assertions.assertThrows(AuthenticationException.class,
                () -> loginServiceMvc.authenticateUser(Mockito.anyString(), Mockito.anyString()));
    }

    @Test
    public void tryGetUser_Should_GetUserFromSession_When_UserIsLogged() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn("luboslav");

        Mockito.when(mockUserService.getByUsername("luboslav"))
                .thenReturn(junkie);

        // Act
        loginServiceMvc.tryGetUser(mockHttpSession);

        // Assert
        Mockito.verify(mockUserService, Mockito.times(1))
                .getByUsername("luboslav");
    }

    @Test
    public void tryGetUser_Should_Throw_When_UserIsNotLogged() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn(null);

        Assertions.assertThrows(AuthorizationException.class,
                () -> loginServiceMvc.tryGetUser(mockHttpSession));
    }

    @Test
    public void tryGetUser_Should_Throw_When_UsernameIsNotLogged() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn("luboslav");

        Mockito.when(mockUserService.getByUsername("luboslav"))
                .thenThrow(new EntityNotFoundException());

        Assertions.assertThrows(AuthorizationException.class,
                () -> loginServiceMvc.tryGetUser(mockHttpSession));
    }

    @Test
    public void verifyAuthorization_Should_ReturnUser_When_UserIsAuthorized() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn("luboslav");

        Mockito.when(loginServiceMvc.tryGetUser(mockHttpSession)).thenReturn(organizer);

        // Act
        var result = loginServiceMvc.verifyAuthorization(mockHttpSession, "organizer");

        // Assert
        assertEquals(result, organizer);
    }

    @Test
    public void verifyAuthorization_Should_Throw_When_NotAuthorized() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn("luboslav");

        Mockito.when(loginServiceMvc.tryGetUser(mockHttpSession)).thenReturn(organizer);

        //Act, Assert
        assertThrows(AuthorizationException.class,
                () -> loginServiceMvc.verifyAuthorization(mockHttpSession, "juror"));
    }

    @Test
    public void verifyAuthorizationJury_Should_ReturnUser_When_UserIsAuthorized() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn("luboslav");

        Mockito.when(loginServiceMvc.tryGetUser(mockHttpSession))
                .thenReturn(organizer);

        // Act
        var result = loginServiceMvc.verifyAuthorizationJury(mockHttpSession, "organizer");

        // Assert
        assertEquals(result, organizer);
    }

    @Test
    public void verifyAuthorizationJury_Should_Throw_When_NotAuthorizedAsJury() {
        //Arrange
        Mockito.when(mockHttpSession.getAttribute("currentUserUsername"))
                .thenReturn("luboslav");

        Mockito.when(loginServiceMvc.tryGetUser(mockHttpSession))
                .thenReturn(junkie);

        //Act, Assert
        assertThrows(AuthorizationException.class,
                () -> loginServiceMvc.verifyAuthorizationJury(mockHttpSession, "juror"));
    }


}
