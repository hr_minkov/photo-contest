package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.validation.UserValidationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockUserRepository;

    @Mock
    UserValidationServiceImpl mockUserValidationService;

    @InjectMocks
    UserServiceImpl userService;

    private static Filter filter;
    private static User junkie;
    private static User organizer;

    @BeforeEach
    private void setup() {
        filter = new Filter();
        junkie = Helpers.createJunkie();
        organizer = Helpers.createOrganizer();
    }

    @Test
    public void getAll_Should_CallRepository() {

        //Arrange
        Mockito.when(mockUserRepository.getAll()).thenReturn(null);

        //Act
        userService.getAll();

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_Should_CallRepository_When_ThereAreFilterParameters() {

        //Arrange
        Mockito.when(mockUserRepository.getAll(filter)).thenReturn(null);

        //Act
        userService.getAll(filter);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll(filter);
    }

    @Test
    public void getById_Should_CallRepository() {

        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(null);

        //Act
        userService.getById(Mockito.anyInt());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getById__Should_Throw_When_UserWithIdNotExists() {
        //Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getById(Mockito.anyInt()));
    }

    @Test
    public void getByUsername_Should_CallRepository() {

        //Arrange
        Mockito.when(mockUserRepository.getActiveByUsername("username")).thenReturn(null);

        //Act
        userService.getByUsername("username");

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getActiveByUsername("username");
    }

    @Test
    public void getByUsername_Should_Throw_When_UsernameNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.getActiveByUsername(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsername(Mockito.anyString()));
    }

    @Test
    public void getByCredentials_Should_CallRepository() {

        //Arrange
        Mockito.when(mockUserRepository.getActiveByCredentials("username", "password"))
                .thenReturn(null);

        //Act
        userService.getByCredentials("username", "password");

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getActiveByCredentials("username", "password");
    }

    @Test
    public void getByCredentials_Should_Throw_When_CredentialsNotExists() {

        //Arrange
        Mockito.when(mockUserRepository.getActiveByCredentials(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByCredentials(Mockito.anyString(),Mockito.anyString()));
    }


    @Test
    public void createJunkie_Should_CallRepository_When_UserDataConsistent() {
        // Arrange
        Mockito.when(mockUserValidationService.ensureUsernameUnique(junkie)).thenReturn(null);
        Mockito.when(mockUserRepository.create(junkie)).thenReturn(null);

        // Act
        userService.create(junkie);

        // Assert
        Mockito.verify(mockUserValidationService, Mockito.times(1)).ensureUsernameUnique(junkie);
        Mockito.verify(mockUserRepository, Mockito.times(1)).create(junkie);
    }

    @Test
    public void createJunkie_Should_Throw_When_UsernameIsNotUnique() {
        // Arrange
        Mockito.when(mockUserValidationService.ensureUsernameUnique(junkie))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userService.create(junkie));
    }


    @Test
    public void update_Should_CallRepository_When_UserDataConsistent() {
        // Arrange
        Mockito.when(mockUserValidationService.ensureUsernameUnique(junkie)).thenReturn(null);
        Mockito.when(mockUserRepository.update(junkie)).thenReturn(null);

        // Act
        userService.update(junkie);

        // Assert
        Mockito.verify(mockUserValidationService, Mockito.times(1)).ensureUsernameUnique(junkie);
        Mockito.verify(mockUserRepository, Mockito.times(1)).update(junkie);
    }

    @Test
    public void updateJunkie_Should_Throw_When_UsernameIsNotUnique() {
        // Arrange
        Mockito.when(mockUserValidationService.ensureUsernameUnique(junkie))
                .thenThrow(new IllegalRequestException());

        // Act, Assert
        assertThrows(IllegalRequestException.class, () -> userService.create(junkie));
    }

    @Test
    public void update_Should_Throw_When_OrganizerTryToUpdateAnotherOrganizer() {
        // Arrange
        Mockito.when(mockUserValidationService.ensureUsernameUnique(organizer))
                .thenThrow(new IllegalRequestException());


        // Act, Assert
        assertThrows(IllegalRequestException.class, () -> userService.update(organizer));
    }

    @Test
    public void delete_Should_CallRepositoryDeleteNonCustomer_When_RequestValid() {
        // Arrange, Act
        userService.delete(junkie);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(junkie);
    }

    @Test
    public void suspend_Should_CallRepository_When_RequestValid() {
        // Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(junkie);
        Mockito.when(mockUserValidationService.ensurePermittedToAlterUserActiveStatus(organizer))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserRepository.delete(junkie)).thenReturn(null);

        // Act
        userService.suspend(junkie, junkie.getUserId());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(junkie);
    }

    @Test
    public void suspend_Should_Throw_When_UserNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(new IllegalRequestException());

        // Act, Assert
        assertThrows(IllegalRequestException.class,
                () -> userService.suspend(junkie, Mockito.anyInt()));
    }

    @Test
    public void suspend_Should_Throw_When_UserNotOrganizer() {
        // Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(junkie);
        Mockito.when(mockUserValidationService.ensurePermittedToAlterUserActiveStatus(junkie))
                .thenThrow(new IllegalRequestException());

        // Act, Assert
        assertThrows(IllegalRequestException.class,
                () -> userService.suspend(junkie, Mockito.anyInt()));
    }

    @Test
    public void suspend_Should_Throw_When_UserNotJunkie() {
        // Arrange
        Mockito.when(mockUserRepository.getById(Mockito.anyInt())).thenReturn(organizer);
        Mockito.when(mockUserValidationService.ensurePermittedToAlterUserActiveStatus(organizer))
                .thenReturn(mockUserValidationService);
        Mockito.when(mockUserValidationService.ensureIsJunkie(junkie))
                .thenThrow(new IllegalRequestException());

        // Act, Assert
        assertThrows(IllegalRequestException.class,
                () -> userService.suspend(junkie, Mockito.anyInt()));
    }

    @Test
    public void restore_Should_CallRepository_When_RequestValid() {
        // Arrange
        junkie.setActive(false);
        Mockito.when(mockUserRepository.getInactiveById(Mockito.anyInt()))
                .thenReturn(junkie);

        // Act
        userService.restore(junkie, junkie.getUserId());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).restore(junkie);
    }


    @Test
    public void restore_Should_Throw_When_UserIsAlreadyActive() {
        // Arrange
        Mockito.when(mockUserRepository.getInactiveById(1))
                .thenThrow(new EntityNotFoundException());

        // Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userService.restore(junkie, junkie.getUserId()));
    }

    @Test
    public void restore_Should_Throw_When_When_UserNotOrganizer() {
        // Arrange
        junkie.setActive(false);
        Mockito.when(mockUserRepository.getInactiveById(Mockito.anyInt()))
                .thenReturn(junkie);
        Mockito.when(mockUserValidationService.ensurePermittedToAlterUserActiveStatus(junkie))
                .thenThrow(new IllegalRequestException());

        // Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userService.restore(junkie, junkie.getUserId()));
    }

    @Test
    public void restore_Should_Throw_When_When_UserAlreadyActive() {
        //Arrange
        Mockito.when(mockUserRepository.getInactiveById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());
        Mockito.when(mockUserRepository.getById(Mockito.anyInt()))
                .thenThrow(new IllegalRequestException());

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userService.restore(junkie, junkie.getUserId()));
    }

    @Test
    public void getAllJunkieEligibleForJurorDuty_Should_CallRepository() {
        //Arrange. Act, Assert
        Assertions.assertDoesNotThrow(() -> userService.getAllJunkiesEligibleForJurorDuty());
    }

    @Test
    public void getPrizeCountForPlace_Should_CallRepository() {
        //Arrange
        Mockito.when(mockUserRepository.getContestAwardsCountFor(Mockito.anyInt(),Mockito.anyInt()))
                .thenReturn(1);

        //Act
        userService.getPrizeCountForPlace(Mockito.anyInt(),Mockito.anyInt());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getContestAwardsCountFor(Mockito.anyInt(),Mockito.anyInt());
    }

    @Test
    public void getAllActiveJunkies_Should_CallRepository() {

        //Arrange
        Mockito.when(mockUserRepository.getAllActiveJunkies()).thenReturn(null);

        //Act
        userService.getAllActiveJunkies();

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAllActiveJunkies();
    }
}
