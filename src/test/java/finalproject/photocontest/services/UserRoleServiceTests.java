package finalproject.photocontest.services;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.repos.contracts.UserRoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserRoleServiceTests {

    @Mock
    UserRoleRepository mockRoleRepo;

    @InjectMocks
    UserRoleServiceImpl roleService;

    @Test
    public void getAll_Should_CallRepository() {

        //Arrange
        Mockito.when(mockRoleRepo.getAll()).thenReturn(null);

        //Act
        roleService.getAll();

        //Assert
        Mockito.verify(mockRoleRepo, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {

        //Arrange
        Mockito.when(mockRoleRepo.getById(Mockito.anyInt())).thenReturn(null);

        //Act
        roleService.getById(1);

        //Assert
        Mockito.verify(mockRoleRepo, Mockito.times(1)).getById(Mockito.anyInt());
    }

    @Test
    public void getById_Should_Throw_When_RoleDoesNotExists() {
        //Arrange
        Mockito.when(mockRoleRepo.getById(Mockito.anyInt())).thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,()->roleService.getById(Mockito.anyInt()));
    }

    @Test
    public void getByName_Should_CallRepository() {

        //Arrange
        Mockito.when(mockRoleRepo.getByName(Mockito.anyString())).thenReturn(null);

        //Act
        roleService.getByName("42");

        //Assert
        Mockito.verify(mockRoleRepo, Mockito.times(1)).getByName(Mockito.anyString());
    }

    @Test
    public void getByName_Should_Throw_When_RoleDoesNotExists() {
        //Arrange
        Mockito.when(mockRoleRepo.getByName(Mockito.anyString())).thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,()->roleService.getByName(Mockito.anyString()));
    }
}
