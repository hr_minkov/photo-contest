package finalproject.photocontest.services;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.ContestWinner;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.ContestRepository;
import finalproject.photocontest.repos.contracts.ContestWinnerRepository;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.utils.ModelMapper;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import finalproject.photocontest.validation.contracts.UserValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.HashSet;

@ExtendWith(MockitoExtension.class)
public class EvaluationServiceTests {

    @Mock
    ContestWinnerRepository mockContestWinnerRepository;

    @Mock
    ContestRepository mockContestRepository;

    @Mock
    PhotoObjectRepository mockPhotoObjectRepository;

    @Mock
    ContestValidationService mockContestValidationService;

    @Mock
    UserValidationService mockUserValidationService;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    ModelMapper mockMapper;

    @Mock
    EvaluationServiceImpl mockEvaluationServiceImpl;

    @Mock
    ContestServiceImpl mockContestService;

    @InjectMocks
    EvaluationServiceImpl evaluationService;

    private User junkie;
    private ContestWinner contestWinner;
    private Contest contest;
    private Photo photo;

    @BeforeEach
    private void setup() {
        junkie = Helpers.createJunkie();
        contestWinner = Helpers.createContestWinner();
        contest = Helpers.createContest();
        photo = Helpers.createPhoto();

    }
}
