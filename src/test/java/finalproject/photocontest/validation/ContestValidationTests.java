package finalproject.photocontest.validation;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.PhotoReviewDto;
import finalproject.photocontest.repos.contracts.ContestRepository;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class ContestValidationTests {

    @Mock
    ContestRepository mockContestRepository;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    PhotoObjectRepository mockPhotoObjectRepository;

    @InjectMocks
    ContestValidationServiceImpl contestValidationService;

    private static User junkie;
    private static User juror;
    private static User organizer;
    private static Contest contest;
    private static Photo photo;


    @BeforeEach
    public void setup() {
        junkie = Helpers.createJunkie();
        juror = Helpers.createJuror();
        organizer = Helpers.createOrganizer();
        contest = new Contest();
        photo = Helpers.createPhoto();
    }

    @Test
    public void ensureUserEnrolled_Should_ValidateUserParticipating_When_ValidParameters() {
        //Arrange, Act, Assert
        contest.setParticipants(Set.of(junkie));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureUserEnrolled(contest, junkie));
    }

    @Test
    public void ensureUserEnrolled_Should_Throw_When_UserIsNotEnrolled() {
        //Arrange, Act, Assert
        juror.setCredentials(Helpers.createCredentialsV2());
        contest.setParticipants(Set.of(juror));
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureUserEnrolled(contest, junkie));
    }

    @Test
    public void ensurePastEnrollmentPhase_Should_NotThrow_When_ContestInEnrollmentPhase() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().minusDays(2));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensurePastEnrollmentPhase(contest));
    }

    @Test
    public void ensurePastEnrollmentPhase_Should_Throw_When_EnrollmentPhasePastOver() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(2));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePastEnrollmentPhase(contest));
    }

    @Test
    public void ensureInEnrollmentPhase_Should_NotThrow_When_ContestInEnrollmentPhase() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().plusDays(2));


        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureInEnrollmentPhase(contest));
    }

    @Test
    public void ensureInEnrollmentPhase_Should_Throw_When_NotInEnrollmentPhase() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().minusDays(2));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureInEnrollmentPhase(contest));
    }

    @Test
    public void ensureFinished_Should_NotThrow_When_ContestFinished() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().minusDays(2));


        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureFinished(contest));
    }

    @Test
    public void ensureFinished_Should_Throw_When_NotInEnrollmentPhase() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().plusDays(2));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureFinished(contest));
    }

    @Test
    public void ensureNotFinished_Should_NotThrow_When_ContestFinished() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().plusDays(2));


        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureNotFinished(contest));
    }

    @Test
    public void ensureNotFinished_Should_Throw_When_NotInEnrollmentPhase() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(2));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().minusDays(2));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureNotFinished(contest));
    }

    @Test
    public void ensureOpen_Should_NotThrow_When_ContestIsOpen() {
        //Arrange, Act, Assert
        contest.setOpen(true);
        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureOpen(contest));
    }

    @Test
    public void ensureOpen_Should_Throw_When_ContestIsInvitational() {
        //Arrange, Act, Assert
        contest.setOpen(false);
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureOpen(contest));
    }

    @Test
    public void ensureTitleUnique_Should_EnsureContestIsUnique_When_ValidParameters() {
        //Arrange
        Mockito.when(mockContestRepository.getByTitle(contest.getTitle()))
                .thenThrow(new EntityNotFoundException());

        //Act
        contestValidationService.ensureTitleUnique(contest);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getByTitle(contest.getTitle());
    }

    @Test
    public void ensureTitleUnique_Should_Throw_When_ContestNameIsNotUnique() {
        //Arrange
        Mockito.when(mockContestRepository.getByTitle(contest.getTitle()))
                .thenReturn(contest);

        //Act, Assure
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureTitleUnique(contest));
    }

    @Test
    public void ensureScheduleTimelineConsistent_Should_NotThrow_When_ContestScheduledValid() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusDays(2));
        contest.setEndPhase2(LocalDateTime.now().plusDays(3));
        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureScheduleTimelineConsistent(contest));
    }

    @Test
    public void ensureScheduleTimelineConsistent_Should_Throw_When_ContestIsInvitational() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(3));
        contest.setStartPhase2(LocalDateTime.now().plusDays(2));
        contest.setEndPhase2(LocalDateTime.now().plusDays(1));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureScheduleTimelineConsistent(contest));
    }

    @Test
    public void ensurePhaseDurationsConsistent_Should_NotThrow_When_DurationConsistent() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusDays(30));
        contest.setEndPhase2(LocalDateTime.now().plusDays(31).minusHours(1));
        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensurePhaseDurationsConsistent(contest));
    }

    @Test
    public void ensurePhaseDurationsConsistent_Should_Throw_When_Phase1DurationIsLessThan1Day() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusHours(21));
        contest.setEndPhase2(LocalDateTime.now().plusDays(1).minusHours(1));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePhaseDurationsConsistent(contest));
    }

    @Test
    public void ensurePhaseDurationsConsistent_Should_Throw_When_Phase1DurationIsMoreThan31Days() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusHours(38));
        contest.setEndPhase2(LocalDateTime.now().plusDays(1).minusHours(1));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePhaseDurationsConsistent(contest));
    }

    @Test
    public void ensurePhaseDurationsConsistent_Should_Throw_When_Phase2DurationIsLessThan1Hour() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusDays(30));
        contest.setEndPhase2(LocalDateTime.now().plusDays(30).plusMinutes(25));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePhaseDurationsConsistent(contest));
    }

    @Test
    public void ensurePhaseDurationsConsistent_Should_Throw_When_Phase2DurationIsLessThan1Day() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusDays(30));
        contest.setEndPhase2(LocalDateTime.now().plusDays(32));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePhaseDurationsConsistent(contest));
    }

    @Test
    public void ensureJurorsEligible_Should_EnsureUserIsJury_When_UserIsEligibleForJuror() {
        //Arrange, Act, Assert
        contest.setJurors(Set.of(juror));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureJurorsEligible(contest));
    }

    @Test
    public void ensureJurorsEligible_Should_EnsureUserIsJury_When_UserIsEligibleOrganizer() {
        //Arrange, Act, Assert
        contest.setJurors(Set.of(organizer));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureJurorsEligible(contest));
    }

    @Test
    public void ensureJurorsEligible_Should_Throw_When_UserNotEligibleForJury() {
        //Arrange, Act, Assert
        contest.setJurors(Set.of(junkie));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureJurorsEligible(contest));
    }

    @Test
    public void ensureNotAlreadyEnrolled_Should_NotThrow_When_UserIsParticipating() {
        //Arrange, Act, Assert
        contest.setParticipants(Set.of());

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureNotAlreadyEnrolled(contest, junkie));
    }

    @Test
    public void ensureNotAlreadyEnrolled_Should_Throw_When_UserIsParticipating() {
        //Arrange, Act, Assert
        contest.setParticipants(Set.of(junkie));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureNotAlreadyEnrolled(contest, junkie));
    }

    @Test
    public void ensureEnrolled_Should_NotThrow_When_UserIsEnrolledForContest() {
        //Arrange, Act, Assert
        contest.setParticipants(Set.of(junkie));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureEnrolled(contest, junkie));
    }

    @Test
    public void ensureEnrolled_Should_Throw_When_UserIsNotEnrolled() {
        //Arrange, Act, Assert
        contest.setParticipants(Set.of());

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureEnrolled(contest, junkie));
    }

    @Test
    public void ensureNotVisibleToUsers_Should_NotThrow_When_UserIsEnrolledForContest() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().plusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusDays(30));
        contest.setEndPhase2(LocalDateTime.now().plusDays(31));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureNotVisibleToUsers(contest));
    }

    @Test
    public void ensureNotVisibleToUsers_Should_Throw_When_UserIsNotEnrolled() {
        //Arrange, Act, Assert
        contest.setStartPhase1(LocalDateTime.now().minusDays(1));
        contest.setStartPhase2(LocalDateTime.now().plusDays(30));
        contest.setEndPhase2(LocalDateTime.now().plusDays(31));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureNotVisibleToUsers(contest));
    }


    @Test
    public void ensurePermissionsToViewContestPhotos_Should_NotThrow_When_UserHasPermission() {
        //Arrange

        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        contest.setJurors(Set.of(juror));
        contest.setParticipants(Set.of());
        contest.setStartPhase1(LocalDateTime.now().minusDays(3));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().minusDays(1));

        //Act
        contestValidationService.ensurePermissionsToViewContestPhotos(1, 1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void ensurePermissionsToViewContestPhotos_Should_Throw_When_UserNotPermittedToViewContest() {
        //Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        contest.setJurors(Set.of());
        contest.setParticipants(Set.of());
        contest.setStartPhase1(LocalDateTime.now().minusDays(3));
        contest.setStartPhase2(LocalDateTime.now().minusDays(2));
        contest.setEndPhase2(LocalDateTime.now().minusDays(1));

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> contestValidationService.ensurePermissionsToViewContestPhotos(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);

    }

    @Test
    public void ensurePermissionsToViewContestPhotos_Should_Throw_When_UserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        Mockito.when(mockUserRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestValidationService.ensurePermissionsToViewContestPhotos(1, 1));
    }


    @Test
    public void ensurePermissionsToViewContestPhotos_Should_Throw_When_ContestDoesNotExists() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestValidationService.ensurePermissionsToViewContestPhotos(1, 1));
    }

    @Test
    public void ensurePhotoPresent_Should_NotThrow_When_PhotoIsUploaded() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockPhotoObjectRepository.getById(1))
                .thenReturn(photo);
        contest.setContestId(1);
        photo.setContest(contest);

        //Act
        contestValidationService.ensurePhotoPresent(1, 1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void ensurePhotoPresent_Should_Throw_When_PhotoNotUploadInContest() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockPhotoObjectRepository.getById(1))
                .thenReturn(photo);
        contest.setContestId(2);
        photo.setContest(contest);


        //Act Assert
        Assertions.assertThrows(IllegalRequestException.class,
                ()->contestValidationService.ensurePhotoPresent(1, 1));
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void ensurePhotoPresent_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenThrow(new EntityNotFoundException());


        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->contestValidationService.ensurePhotoPresent(1, 1));
    }

    @Test
    public void ensurePhotoPresent_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockPhotoObjectRepository.getById(1))
                .thenThrow(new EntityNotFoundException());


        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->contestValidationService.ensurePhotoPresent(1, 1));
    }



    //---------------------------------

























    //---
    @Test
    public void ensureUserUploadedPhoto_Should_ValidateUserIsNotParticipant_When_UserIsJuryInSameContest() {
        //Arrange, Act, Assert
        contest.setPhotos(Set.of(photo));
        contest.setParticipants(Set.of(junkie));

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureUserUploadedPhoto(junkie, contest));
    }

    @Test
    public void ensureUserUploadedPhoto_Should_Throw_When_UserIsJury() {
        //Arrange, Act, Assert
        contest.setPhotos(Set.of());

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureUserUploadedPhoto(junkie, contest));
    }

    @Test
    public void ensureUserHasNoPhotoUploaded_Should_NotThrow_When_UserNotUploadedPhoto() {
        ///Arrange, Act, Assert
        contest.setContestId(1);
        photo.setAuthor(organizer);
        contest.setPhotos(Set.of(photo));
        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensureUserHasNoPhotoUploaded(junkie, contest));
    }

    @Test
    public void ensureUserHasNoPhotoUploaded_Should_Throw_When_UserAlreadyUploadedPhoto() {
        ///Arrange, Act, Assert
        contest.setContestId(1);
        photo.setAuthor(junkie);
        contest.setPhotos(Set.of(photo));
        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensureUserHasNoPhotoUploaded(junkie, contest));
    }

    @Test
    public void ensurePermissionsToViewContest_Should_NotThrow_When_UserHasPermission() {
        //Arrange, Act, Assert

        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        contest.setJurors(Set.of(juror));
        contest.setParticipants(Set.of());

        //Act
        contestValidationService.ensurePermissionsToViewContest(1, 1);

        //Assert
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);
    }

    @Test
    public void ensurePermissionsToViewContest_Should_Throw_When_UserNotPermittedToViewContest() {
        //Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);

        contest.setJurors(Set.of());
        contest.setParticipants(Set.of());

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> contestValidationService.ensurePermissionsToViewContest(1, 1));
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getById(1);
        Mockito.verify(mockContestRepository, Mockito.times(1))
                .getById(1);

    }

    @Test
    public void ensurePermissionsToViewContest_Should_Throw_When_UserDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestValidationService.ensurePermissionsToViewContest(1, 1));
    }

    @Test
    public void ensurePermissionsToViewContest_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(juror);
        Mockito.when(mockContestRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestValidationService.ensurePermissionsToViewContest(1, 1));
    }

    @Test
    public void ensurePermissionsToViewContest_Should_Throw_When_Contest() {
        //Arrange
        Mockito.when(mockContestRepository.getById(1))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestValidationService.ensurePermissionsToViewContest(1, 1));
    }

    @Test
    public void ensurePhotoReviewConsistent_Should_NotThrow_When_UserNotUploadedPhoto() {
        ///Arrange, Act, Assert
        PhotoReviewDto dto = new PhotoReviewDto();
        dto.setScore(5);
        dto.setComment("comment");
        dto.setValid(true);

        Assertions.assertDoesNotThrow(
                () -> contestValidationService.ensurePhotoReviewConsistent(dto));
    }

    @Test
    public void ensurePhotoReviewConsistentShould_Throw_When_PhotoNotRelevantForContest() {
        ///Arrange, Act, Assert
        ///Arrange, Act, Assert
        PhotoReviewDto dto = new PhotoReviewDto();
        dto.setScore(0);
        dto.setComment("comment");
        dto.setValid(true);

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePhotoReviewConsistent(dto));
    }


    @Test
    public void ensurePhotoReviewConsistentShould_Throw_When_EmptyComment() {
        ///Arrange, Act, Assert
        ///Arrange, Act, Assert
        PhotoReviewDto dto = new PhotoReviewDto();
        dto.setScore(5);
        dto.setComment("");
        dto.setValid(true);

        Assertions.assertThrows(IllegalRequestException.class,
                () -> contestValidationService.ensurePhotoReviewConsistent(dto));
    }


}
