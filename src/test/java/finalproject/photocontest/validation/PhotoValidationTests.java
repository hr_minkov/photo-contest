package finalproject.photocontest.validation;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.AuthorizationException;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.PhotoEvaluation;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.PhotoReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class PhotoValidationTests {
    @Mock
    PhotoReviewRepository mockPhotoReviewRepository;

    @Mock
    private static MockMultipartFile mockMultipartFile;

    @Mock
    private static PhotoObjectRepository mockPhotoObjectRepository;

    @InjectMocks
    PhotoValidationServiceImpl photoValidationService;

    private static User junkie;
    private static User juror;
    private static ArrayList<User> jurors;
    private static Contest contest;
    private static PhotoEvaluation photoEvaluation;
    private static Photo photo;


    @BeforeEach
    public void setup() {
        junkie = Helpers.createJunkie();
        juror = Helpers.createJuror();
        jurors = new ArrayList<>();
        jurors.add(juror);

        contest = Helpers.createContest();
        photo = Helpers.createPhoto();
        photoEvaluation = Helpers.createPhotoEvaluation();
    }

    @Test
    public void ensureUnique_Should_EvaluatePhoto_When_ValidParameters() {
        //Arrange
        Mockito.when(mockPhotoReviewRepository.getPhotoReviewByJurorAndPhotoId(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act
        photoValidationService.ensureUnique(photoEvaluation);

        //Assert
        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1))
                .getPhotoReviewByJurorAndPhotoId(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    public void ensureUnique_Should_Throw_When_PhotoAlreadyEvaluated() {
        //Arrange
        Mockito.when(mockPhotoReviewRepository.getPhotoReviewByJurorAndPhotoId(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(null);

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> photoValidationService.ensureUnique(photoEvaluation));
    }

//    @Test
//    public void ensureFileNotEmpty_Should_EnsureFileNotEmpty_When_ValidParameter() throws IOException {
//        //Arrange, Act, Assert
//        FileInputStream inputFile = new FileInputStream( "D:\\TelerikAlpha\\repository\\photo-contest\\PhotoContest\\src\\main\\resources\\static\\images\\photo-contest\\1.1.jpg");
//        MockMultipartFile file = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", inputFile);
//
//        Assertions.assertDoesNotThrow(
//                () -> photoValidationService.ensureFileNotEmpty(file));
//    }
//
//    @Test
//    public void ensureFileNotEmpty_Should_Throw_When_FileIsEmpty() throws IOException {
//        //Arrange, Act, Assert
//        Assertions.assertThrows(IllegalRequestException.class,
//                () -> photoValidationService.ensureFileNotEmpty(mockMultipartFile));
//    }

    @Test
    public void ensurePermissionsToViewPhoto_Should_ViewOwnPhoto_When_UserIsAuthorized() {

        //Arrange
        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt()))
                .thenReturn(photo);

        //Act
        photoValidationService.ensurePermissionsToViewPhoto(1, 1, junkie);

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void ensurePermissionsToViewPhoto_Should_ViewOthersPhoto_When_ContestIsFinished() {
        //Arrange
        User junkie2 = Helpers.createJunkie();
        junkie2.setUserId(2);
        contest.setParticipants(Set.of(junkie2));

        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt()))
                .thenReturn(photo);
        photo.getContest().setStartPhase1(LocalDateTime.now().minusDays(10));
        photo.getContest().setStartPhase2(LocalDateTime.now().minusDays(5));
        photo.getContest().setEndPhase2(LocalDateTime.now().minusDays(2));


        //Act
        photoValidationService.ensurePermissionsToViewPhoto(1, 1, junkie2);

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void ensurePermissionsToViewPhoto_Should_ViewOthersPhoto_When_UserIsJuryInPhaseI() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt()))
                .thenReturn(photo);
        photo.getContest().setStartPhase1(LocalDateTime.now().minusDays(10));
        photo.getContest().setStartPhase2(LocalDateTime.now().minusDays(5));
        photo.getContest().setEndPhase2(LocalDateTime.now().plusDays(6));


        //Act
        photoValidationService.ensurePermissionsToViewPhoto(1, 1, juror);

        //Assert
        Mockito.verify(mockPhotoObjectRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void ensurePermissionsToViewPhoto_Should_Throw_When_UserIsNotAuthorized() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt()))
                .thenReturn(photo);
        photo.getContest().setStartPhase1(LocalDateTime.now().minusDays(10));
        photo.getContest().setStartPhase2(LocalDateTime.now().plusDays(5));
        photo.getContest().setEndPhase2(LocalDateTime.now().plusDays(2));

        //Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> photoValidationService.ensurePermissionsToViewPhoto(1, 1, juror));
    }

    @Test
    public void ensurePermissionsToViewPhoto_Should_Throw_When_PhotoDoesNotExist() {
        //Arrange
        Mockito.when(mockPhotoObjectRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> photoValidationService.ensurePermissionsToViewPhoto(1, 1, juror));
    }

    @Test
    public void ensurePhotoDataConsistent_Should_CreatePhoto_When_ValidParameters() {
        //Arrange, Act, Assert
        Assertions.assertDoesNotThrow(
                () -> photoValidationService.ensurePhotoDataConsistent("TITLE","STORY"));
    }
}
