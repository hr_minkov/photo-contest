package finalproject.photocontest.validation;

import finalproject.photocontest.Helpers;
import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Credentials;
import finalproject.photocontest.models.User;
import finalproject.photocontest.repos.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class UserValidationTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserValidationServiceImpl userValidationService;

    private static User organizer;
    private static User junkie;
    private static User juror;
    private static ArrayList<User> jurors;
    private static Contest contest;

    @BeforeEach
    public void setup() {
        organizer = Helpers.createOrganizer();
        junkie = Helpers.createJunkie();
        juror = Helpers.createJuror();

        jurors = new ArrayList<>();
        jurors.add(juror);

        contest = Helpers.createContest();
    }


    @Test
    public void ensureUsernameUnique_Should_ValidateUserNameIsUnique_When_ValidParameter() {
        //Arrange
        Mockito.when(userRepository.getByUsername(junkie.getCredentials().getUsername()))
                .thenReturn(junkie);

        //Act
        userValidationService.ensureUsernameUnique(junkie);

        //Assertions
        Mockito.verify(userRepository, Mockito.times(1))
                .getByUsername(junkie.getCredentials().getUsername());

    }

    @Test
    public void ensureUsernameUnique_Should_Throw_When_UsernameIsNotUnique() {
        //Arrange
        User junkieTwo = Helpers.createJunkie();
        junkieTwo.setUserId(-1);
        Mockito.when(userRepository.getByUsername(junkie.getCredentials().getUsername()))
                .thenReturn(junkieTwo);

        //Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userValidationService.ensureUsernameUnique(junkie));
    }

    @Test
    public void ensureUsernameUnique_Should_Throw_When_UsernameIsUnique() {
        //Arrange
        Mockito.when(userRepository.getByUsername(junkie.getCredentials().getUsername()))
                .thenThrow(new EntityNotFoundException());


        //Act
        userValidationService.ensureUsernameUnique(junkie);

        //Assertions
        Mockito.verify(userRepository, Mockito.times(1))
                .getByUsername(junkie.getCredentials().getUsername());
    }

    @Test
    public void ensureIsJunkie_Should_ValidateJurors_When_allUsersAreJurors() {
        //Arrange, Act, Assert
        Assertions.assertDoesNotThrow(
                () -> userValidationService.ensureIsJunkie(junkie));
    }

    @Test
    public void ensureIsJunkie_Should_Throw_When_UserIsNotJunkie() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userValidationService.ensureIsJunkie(organizer));
    }

    @Test
    public void ensurePermittedToAlterUserActiveStatus_Should_AllowToChangeStatus_When_UserIsOrganizer() {
        //Arrange, Act, Assert
        Assertions.assertDoesNotThrow(
                () -> userValidationService.ensurePermittedToAlterUserActiveStatus(organizer));
    }

    @Test
    public void ensurePermittedToAlterUserActiveStatus_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userValidationService.ensurePermittedToAlterUserActiveStatus(junkie));
    }

    @Test
    public void ensureIsEligibleForJuror_Should_EnsureUserIsEligible_When_UserIsJurror() {
        //Arrange, Act, Assert
        Assertions.assertDoesNotThrow(
                () -> userValidationService.ensureIsEligibleForJuror(juror));
    }

    @Test
    public void ensureIsEligibleForJuror_Should_Throw_When_UserIsNotJuror() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalRequestException.class,
                () -> userValidationService.ensureIsEligibleForJuror(junkie));
    }

    @Test
    public void ensureIsNotJuror_Should_ValidateUserIsNotParticipant_When_UserIsJuryInSameContest() {
        //Arrange, Act, Assert
        Credentials credentials = new Credentials("asdasdasd", "adadsad");
        juror.setCredentials(credentials);

        contest.setJurors(Set.of(juror));

        Assertions.assertDoesNotThrow(
                () -> userValidationService.ensureIsNotJuror(junkie, contest));
    }

    @Test
    public void ensureIsNotJuror_Should_Throw_When_UserIsJury() {
        //Arrange, Act, Assert
        contest.setJurors(Set.of(juror));

        Assertions.assertThrows(IllegalRequestException.class,
                () -> userValidationService.ensureIsNotJuror(juror, contest));
    }


}
