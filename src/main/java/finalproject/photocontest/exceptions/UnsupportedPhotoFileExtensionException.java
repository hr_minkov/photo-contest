package finalproject.photocontest.exceptions;

public class UnsupportedPhotoFileExtensionException extends PhotoStorageException{

    public UnsupportedPhotoFileExtensionException(String message) {
        super(message);
    }
}
