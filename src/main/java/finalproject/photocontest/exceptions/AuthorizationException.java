package finalproject.photocontest.exceptions;

public class AuthorizationException extends RuntimeException {

    public AuthorizationException(String username) {
        super(String.format("Authorization failed for user '%s'", username));
    }
}
