package finalproject.photocontest.constants.repositories;

public class PhotoRepositoryConstants {
    public static final String FAILED_TO_STORE_FILE_EX_MESSAGE = "Failed to store file";
    public static final String COULD_NOT_READ_FILE_EX_MESSAGE = "Could not read file: ";
    public static final String FAILED_TO_DELETE_EX_MESSAGE = "Failed to delete ";
}
