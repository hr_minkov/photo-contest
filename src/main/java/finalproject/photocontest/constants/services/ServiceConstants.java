package finalproject.photocontest.constants.services;

public class ServiceConstants {
    public static final String EVALUATION_USER_SCORE_NOT_IN_SCORES_LIST_EX_MESSAGE = "User score not in scores list for some reason. #FIXME :)";
    public static final String EVALUATION_PHOTOS_MORE_THAN_PARTICIPANTS_EX_MESSAGE = "Photos more than participants #FIXME :)";
    public static final String EVALUATION_DEFAULT_COMMENT = "This is a default comment. Photo not evaluated by juror";
    public static final String USER_ALREADY_HAS_ACTIVE_STATUS_EX_MESSAGE = "User already has active status";
    public static final int FIRST_PLACE = 1;
    public static final int SECOND_PLACE = 2;
    public static final int THIRD_PLACE = 3;
    public static final int POINTS_FIRST_PLACE_SINGLE = 50;
    public static final int POINTS_FIRST_PLACE_SHARED = 40;
    public static final int POINTS_FIRST_PLACE_DOUBLED = 75;
    public static final int POINTS_SECOND_PLACE_SINGLE = 35;
    public static final int POINTS_THIRD_PLACE_SINGLE = 20;
    public static final int POINTS_SECOND_PLACE_SHARED = 25;
    public static final int POINTS_THIRD_PLACE_SHARED = 10;
}
