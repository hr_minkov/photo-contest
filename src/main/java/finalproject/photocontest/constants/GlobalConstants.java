package finalproject.photocontest.constants;

public class GlobalConstants {

    //ExceptionHandler
    public static final String ENTITY_NOT_FOUND_EXCEPTION = "EntityNotFoundException";
    public static final String DUPLICATE_ENTITY_EXCEPTION = "DuplicateEntityException";
    public static final String AUTHORIZATION_EXCEPTION = "AuthorizationException";
    public static final String AUTHENTICATION_EXCEPTION = "AuthenticationException";
    public static final String ILLEGAL_REQUEST_EXCEPTION = "IllegalRequestException";
    public static final String PHOTO_STORAGE_EXCEPTION = "PhotoStorageException";
    public static final String FILE_EXTENSION_EXCEPTION = "UnsupportedPhotoFileExtensionException";

    //LoginHandler
    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";

    //User
    public static final String ROLE_ORGANIZER = "Organizer";
    public static final String ROLE_JUNKIE = "Junkie";
    public static final int JUNKIE_RANK_STARTING_POINTS = 0;
    public static final int ENTHUSIAST_RANK_STARTING_POINTS = 51;
    public static final int MASTER_RANK_STARTING_POINTS = 151;
    public static final int WBPD_RANK_STARTING_POINTS = 1001;
}
