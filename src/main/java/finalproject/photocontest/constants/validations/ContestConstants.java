package finalproject.photocontest.constants.validations;

public class ContestConstants {

    public static final String CONTEST_FINISHED_EX_MESSAGE = "Contest already finished.";
    public static final String CONTEST_INVITATIONAL_ONLY_EX_MESSAGE = "This contest is only for invited members";
    public static final String CONTEST_USER_ALREADY_ENROLLED_EX_MESSAGE = "This user already participate in this contest.";
    public static final String CONTEST_USER_NOT_ENROLLED_EX_MESSAGE = "User not enrolled for this contest";
    public static final String CONTEST_NOT_PAST_ENROLLMENT_EX_MESSAGE = "Contest not past enrollment phase yet";
    public static final String CONTEST_PAST_ENROLLMENT_PHASE_EX_MESSAGE = "Contest past enrollment phase";
    public static final String CONTEST_NOT_FINISHED_EX_MESSAGE = "Contest not finished yet";
    public static final String CONTEST_TITLE_ALREADY_EXISTS_EX_MESSAGE = "Contest with such title already exists";
    public static final String CONTEST_PHASES_NOT_SCHEDULED_CORRECT_EX_MESSAGE = "Contest phases are not scheduled in a correct timeline";
    public static final String CONTEST_DURATION_OF_PHASE_I_EX_MESSAGE = "The duration of Phase I must be 1 - 31 days";
    public static final String CONTEST_DURATION_OF_PHASE_II_EX_MESSAGE = "The duration of Phase II must be 1 - 24 hours";
    public static final String CONTEST_USER_NOT_ELIGIBLE_AS_JUROR_EX_MESSAGE = "User is not eligible as juror";
    public static final String CONTEST_CANNOT_DELETE_ALREADY_VISIBLE_EX_MESSAGE = "Cannot delete contest that is already visible";
    public static final String CONTEST_PHOTO_DOES_NOT_EXIST_EX_MESSAGE = "Photo does not exist in this contest";
    public static final String CONTEST_USER_ALREADY_UPLOADED_PHOTO_EX_MESSAGE = "User has already uploaded a photo for this contest";
}
