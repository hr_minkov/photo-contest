package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.UpdateUserDto;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.UserService;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static finalproject.photocontest.constants.GlobalConstants.*;

@RestController
@RequestMapping(value = "/api/v1/dashboard", headers = {PASSWORD, USERNAME})
public class DashboardCommonControllerRest {

    private final LoginServiceImpl loginService;
    private final UserService userService;
    private final ModelMapper mapper;

    @Autowired
    public DashboardCommonControllerRest(LoginServiceImpl loginService,
                                         UserService userService,
                                         ModelMapper mapper) {

        this.loginService = loginService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/users/{userId}")
    public User serveSingleUserProfile(@PathVariable int userId, @RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER, ROLE_JUNKIE);
            return userService.getById(userId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/users/profile")
    public User serveUserOwnProfile(@RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE, ROLE_ORGANIZER);

            return userService.getById(authenticatedUser.getUserId());

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PutMapping("/users/profile")
    public User handleUserProfileUpdate(@RequestHeader HttpHeaders headers, @Valid @RequestBody UpdateUserDto dto) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE, ROLE_ORGANIZER);

            var user = mapper.fromDto(dto, authenticatedUser.getUserId());
            return userService.update(user);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @DeleteMapping("/users/profile")
    public User handleUserAccountDeletion(@RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE, ROLE_ORGANIZER);

            return userService.delete(authenticatedUser);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}
