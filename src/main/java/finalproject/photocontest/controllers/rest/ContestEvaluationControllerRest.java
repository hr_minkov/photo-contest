package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.PhotoEvaluation;
import finalproject.photocontest.models.dtos.PhotoReviewDto;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.services.contracts.PhotoService;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.utils.ModelMapper;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static finalproject.photocontest.constants.GlobalConstants.*;
import static finalproject.photocontest.models.enums.FilterOption.*;

@RestController
@RequestMapping(value = "/api/v1/contests", headers = {PASSWORD, USERNAME})
public class ContestEvaluationControllerRest {

    private final LoginServiceImpl loginService;
    private final PhotoService photoService;
    private final EvaluationService evaluationService;
    private final ContestValidationService contestValidationService;
    private final ModelMapper mapper;

    @Autowired
    public ContestEvaluationControllerRest(LoginServiceImpl loginService, PhotoService photoService,
                                           EvaluationService evaluationService,
                                           ContestValidationService contestValidationService, ModelMapper mapper) {
        this.loginService = loginService;
        this.photoService = photoService;
        this.evaluationService = evaluationService;
        this.contestValidationService = contestValidationService;
        this.mapper = mapper;
    }

    @GetMapping("/{contestId}/review/photos")
    public List<Photo> listAllNotEvaluatedPhotoObjects(@PathVariable int contestId,
                                                       @RequestHeader HttpHeaders headers,
                                                       @RequestParam Optional<Integer> authorId,
                                                       @RequestParam Optional<String> search) {
        try {
            var user = loginService.authenticateUser(headers);
            loginService.authorizeRole(user, ROLE_JUNKIE, ROLE_ORGANIZER);
            contestValidationService.ensurePermissionsToViewContestPhotos(user.getUserId(), contestId);

            var filter = new Filter()
                    .add(CONTEST_ID, String.valueOf(contestId))
                    .add(JUROR_ID, String.valueOf(user.getUserId()));

            authorId.ifPresent(author -> filter.add(AUTHOR_ID, String.valueOf(author)));
            search.ifPresent(s -> filter.add(SEARCH, s));

            return photoService.getAll(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PostMapping("/{contestId}/review/photos/{photoId}")
    public PhotoEvaluation handlePhotoEvaluation(@PathVariable int contestId, @PathVariable int photoId,
                                                 @RequestBody PhotoReviewDto dto,
                                                 @RequestHeader HttpHeaders headers) {

        try {
            var user = loginService.authenticateUser(headers);
            loginService.authorizeRole(user, ROLE_JUNKIE, ROLE_ORGANIZER);
            contestValidationService.ensurePermissionsToViewContestPhotos(user.getUserId(), contestId);

            var photoReview =
                    mapper.fromDto(dto, user.getUserId(), contestId, photoId);

            return evaluationService.create(photoReview);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}
