package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.daos.UserRankingInfoDao;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.PhotoService;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static finalproject.photocontest.constants.GlobalConstants.*;
import static finalproject.photocontest.models.enums.FilterOption.*;

@RestController
@RequestMapping(value = "/api/v1/dashboard", headers = {PASSWORD, USERNAME})
public class DashboardJunkieControllerRest {

    private final LoginServiceImpl loginService;
    private final ModelMapper mapper;
    private final ContestService contestService;
    private final PhotoService photoService;

    @Autowired
    public DashboardJunkieControllerRest(ModelMapper mapper, ContestService contestService,
                                         LoginServiceImpl loginService, PhotoService photoService) {
        this.mapper = mapper;
        this.contestService = contestService;
        this.loginService = loginService;
        this.photoService = photoService;
    }

    @GetMapping("/ranking")
    public UserRankingInfoDao serveUserOwnRankingInfo(@RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);

            return mapper.compileRankingInfoFor(authenticatedUser);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/open-contests")
    public List<Contest> listAllAvailableContests(@RequestHeader HttpHeaders headers,
                                                  @RequestParam Optional<Integer> categoryId,
                                                  @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);
            var filter = new Filter().add(USER_ID, String.valueOf(authenticatedUser.getUserId()));
            categoryId.ifPresent(cat -> filter.add(CATEGORY_ID, String.valueOf(cat)));
            search.ifPresent(s -> filter.add(SEARCH, s));

            return contestService.getContestsAvailableToUser(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/my-contests")
    public List<Contest> listAllContestsUserParticipatesIn(@RequestHeader HttpHeaders headers,
                                                           @RequestParam Optional<String> phase,
                                                           @RequestParam Optional<Integer> categoryId,
                                                           @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);

            var filter = new Filter()
                    .add(USER_ID, String.valueOf(authenticatedUser.getUserId()));
            phase.ifPresent(ph -> filter.add(PHASE, ph));
            categoryId.ifPresent(cat -> filter.add(CATEGORY_ID, String.valueOf(cat)));
            search.ifPresent(s -> filter.add(SEARCH, s));

            return contestService.getAll(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/my-photos")
    public List<Photo> lisAllUserPhotos(@RequestHeader HttpHeaders headers,
                                        @RequestParam Optional<String> categoryId,
                                        @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);

            var filter = new Filter()
                    .add(AUTHOR_ID, String.valueOf(authenticatedUser.getUserId()));
            categoryId.ifPresent(category -> filter.add(CATEGORY_ID, category));
            search.ifPresent(s -> filter.add(SEARCH, s));

            return photoService.getAll(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}
