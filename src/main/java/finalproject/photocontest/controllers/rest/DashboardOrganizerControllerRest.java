package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.dtosRest.CreateContestDto;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.UserService;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static finalproject.photocontest.constants.GlobalConstants.*;
import static finalproject.photocontest.models.enums.FilterOption.*;

@RestController
@RequestMapping(value = "/api/v1/dashboard", headers = {PASSWORD, USERNAME})
public class DashboardOrganizerControllerRest {

    private final LoginServiceImpl loginService;
    private final ModelMapper mapper;
    private final ContestService contestService;
    private final UserService userService;

    @Autowired
    public DashboardOrganizerControllerRest(ModelMapper mapper, ContestService contestService,
                                            LoginServiceImpl loginService, UserService userService) {
        this.mapper = mapper;
        this.contestService = contestService;
        this.loginService = loginService;
        this.userService = userService;
    }

    @GetMapping("/contests")
    public List<Contest> listAllContests(@RequestHeader HttpHeaders headers, @RequestParam Optional<String> userId,
                                         @RequestParam Optional<String> phase, @RequestParam Optional<String> status,
                                         @RequestParam Optional<String> categoryId, @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);

            var filter = new Filter();
            phase.ifPresent(ph -> filter.add(PHASE, ph));
            status.ifPresent(st -> filter.add(STATUS, st));
            userId.ifPresent(user -> filter.add(USER_ID, user));
            categoryId.ifPresent(cat -> filter.add(CATEGORY_ID, cat));
            search.ifPresent((sr -> filter.add(SEARCH, sr)));

            return contestService.getAll(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/users")
    public List<User> listAllUsers(@RequestHeader HttpHeaders headers, @RequestParam Optional<String> role,
                                  @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);

            var filter = new Filter();
            role.ifPresent(rol -> filter.add(ROLE, rol));
            search.ifPresent(sr -> filter.add(SEARCH, sr));

            return userService.getAll(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/junkie-jurors")
    public List<User> listAllJunkiesEligibleForJuryDuty(@RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);

            return userService.getAllJunkiesEligibleForJurorDuty();

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @DeleteMapping("/users/{userId}")
    public User handleUserProfileSuspension(@PathVariable int userId, @RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);
            return userService.suspend(authenticatedUser, userId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PostMapping("/users/{userId}")
    public User handleUserProfileRestoration(@PathVariable int userId,
                                             @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);
            return userService.restore(authenticatedUser, userId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PostMapping("/contests/{contestId}/users/{userId}")
    public Contest handleUserInvitation(@PathVariable int userId, @PathVariable int contestId,
                                        @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);
            return contestService.inviteUserToContest(contestId, userId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PostMapping("/contests")
    public Contest handleContestCreation(@RequestBody @Valid CreateContestDto dto,
                                         @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);
            var contest = mapper.fromDto(dto);
            return contestService.create(contest);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @DeleteMapping("/contests/{contestId}")
    public Contest handleContestDeletion(@PathVariable int contestId,
                                         @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER);
            return contestService.delete(contestId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}
