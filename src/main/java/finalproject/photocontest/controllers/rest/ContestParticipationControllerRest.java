package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.daos.PhotoEvaluationDao;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.services.contracts.PhotoService;
import finalproject.photocontest.services.contracts.UserService;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.validation.contracts.PhotoValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static finalproject.photocontest.constants.GlobalConstants.*;

@RestController
@RequestMapping(value = "/api/v1/contests", headers = {PASSWORD, USERNAME})
public class ContestParticipationControllerRest {

    private final LoginServiceImpl loginService;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final UserService userService;
    private final EvaluationService evaluationService;
    private final PhotoValidationService photoValidationService;

    @Autowired
    public ContestParticipationControllerRest(ContestService contestService, LoginServiceImpl loginService,
                                              PhotoService photoService, UserService userService,
                                              EvaluationService evaluationService,
                                              PhotoValidationService photoValidationService) {
        this.contestService = contestService;
        this.loginService = loginService;
        this.photoService = photoService;
        this.userService = userService;
        this.evaluationService = evaluationService;
        this.photoValidationService = photoValidationService;
    }

    @GetMapping("/{contestId}/evaluation")
    public PhotoEvaluationDao serveUserPhotoEvaluation(@PathVariable int contestId,
                                                       @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);

            return evaluationService.getSinglePhotoEvaluation(authenticatedUser.getUserId(), contestId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/{contestId}/evaluations")
    public List<PhotoEvaluationDao> listAllContestPhotoEvaluations(@PathVariable int contestId,
                                                                   @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);

            return evaluationService.getAllContestPhotoEvaluations(authenticatedUser.getUserId(), contestId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PostMapping("/{contestId}/enroll")
    public Contest handleUserEnrollment(@PathVariable int contestId, @RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);
            return contestService.enroll(contestId, authenticatedUser.getUserId());

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @DeleteMapping("/{contestId}/enroll")
    public Contest handleUserEnrollmentCancellation(@PathVariable int contestId,
                                                    @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);
            return contestService.cancelEnrollment(contestId, authenticatedUser.getUserId());

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @PostMapping("/{contestId}/photos")
    public Photo handleUserPhotoUpload(@PathVariable int contestId, @RequestHeader HttpHeaders headers,
                                       @RequestPart("Title") String title, @RequestPart("Story") String story,
                                       @RequestPart("File") MultipartFile file) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);
            photoValidationService.ensurePhotoDataConsistent(title, story);

            var photo = new Photo(title, story,
                    userService.getById(authenticatedUser.getUserId()),
                    contestService.getById(contestId));
            return photoService.create(photo, file);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @DeleteMapping("/{contestId}/photos")
    public Contest handleUserPhotoRemoval(@PathVariable int contestId, @RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE);
            return contestService.removePhoto(contestId, authenticatedUser.getUserId());

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}
