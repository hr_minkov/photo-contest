package finalproject.photocontest.controllers.rest;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.services.LoginServiceImpl;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.PhotoService;
import finalproject.photocontest.utils.ExceptionHandler;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

import static finalproject.photocontest.constants.GlobalConstants.*;
import static finalproject.photocontest.models.enums.FilterOption.*;

@RestController
@RequestMapping(value = "/api/v1/contests", headers = {PASSWORD, USERNAME})
public class ContestCommonControllerRest {

    private final LoginServiceImpl loginService;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final ContestValidationService contestValidationService;

    @Autowired
    public ContestCommonControllerRest(ContestService contestService, LoginServiceImpl loginService,
                                       PhotoService photoService, ContestValidationService contestValidationService) {

        this.contestService = contestService;
        this.loginService = loginService;
        this.photoService = photoService;
        this.contestValidationService = contestValidationService;
    }

    @GetMapping("/{contestId}/countdown")
    public Duration serveTimeTillNextPhase(@PathVariable int contestId, @RequestHeader HttpHeaders headers) {

        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER, ROLE_JUNKIE);

            return contestService.getTimeTillNextPhase(contestId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/{contestId}")
    public Contest serveSingleContestById(@PathVariable int contestId,
                                          @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER, ROLE_JUNKIE);
            contestValidationService.ensurePermissionsToViewContest(contestId, authenticatedUser.getUserId());

            return contestService.getById(contestId);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/{contestId}/photos/{photoId}")
    public Photo serveSingleContestPhotoObjectById(@PathVariable int contestId, @PathVariable int photoId,
                                                   @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER, ROLE_JUNKIE);

            return photoService.getByContestIdAndPhotoId(contestId, photoId, authenticatedUser);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping(value = "/{contestId}/photos/{photoId}/rsc",
            produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
    public ResponseEntity<Resource> serveSingleContestPhotoFileById(@PathVariable int contestId,
                                                                    @PathVariable int photoId,
                                                                    @RequestHeader HttpHeaders headers) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_JUNKIE, ROLE_ORGANIZER);

            var file = photoService.loadContestPhotoFileAsResource(contestId, photoId, authenticatedUser);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + file.getFilename() + "\"").body(file);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }

    @GetMapping("/{contestId}/photos")
    public List<Photo> listAllContestPhotoObjects(@PathVariable int contestId, @RequestHeader HttpHeaders headers,
                                                  @RequestParam Optional<String> authorId,
                                                  @RequestParam Optional<String> search) {
        try {
            var authenticatedUser = loginService.authenticateUser(headers);
            loginService.authorizeRole(authenticatedUser, ROLE_ORGANIZER, ROLE_JUNKIE);
            contestValidationService.ensurePermissionsToViewContestPhotos(authenticatedUser.getUserId(), contestId);

            var filter = new Filter().add(CONTEST_ID, String.valueOf(contestId));
            authorId.ifPresent(author -> filter.add(AUTHOR_ID, author));
            search.ifPresent(s -> filter.add(SEARCH, s));

            return photoService.getAll(filter);

        } catch (RuntimeException e) {
            throw ExceptionHandler.process(e);
        }
    }
}

