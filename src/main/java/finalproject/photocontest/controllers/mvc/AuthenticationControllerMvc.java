package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.exceptions.AuthenticationException;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.dtosMvc.LoginDto;
import finalproject.photocontest.services.LoginServiceMvcImpl;
import finalproject.photocontest.services.contracts.EvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping
public class AuthenticationControllerMvc {


    private final LoginServiceMvcImpl loginService;
    private final EvaluationService evaluationService;

    @Autowired
    public AuthenticationControllerMvc(LoginServiceMvcImpl loginService, EvaluationService evaluationService) {
        this.loginService = loginService;
        this.evaluationService = evaluationService;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new LoginDto());
        model.addAttribute("topPhotos", evaluationService.getTopPhotos(5));
        model.addAttribute("topRanked", evaluationService.getTopUsers(9));
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDto loginDto,
                              BindingResult bindingResult,
                              HttpSession session,
                              Model model) {

        if (bindingResult.hasErrors()) return "login";

        User user;
        try {
            user = loginService.authenticateUser(loginDto.getUsername(), loginDto.getPassword());
            session.setAttribute("currentUserUsername", loginDto.getUsername());
        } catch (AuthenticationException e) {
            bindingResult.rejectValue("password", "auth_error", e.getMessage());
            return "login";
        }

        return user.isJunkie()
                ? "redirect:/dashboard/open"
                : "redirect:/dashboard/contests";

    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUserUsername");
        return "redirect:/";
    }
}
