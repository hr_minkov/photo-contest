package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.dtos.dtosMvc.OrganizerContestFilterDto;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.models.enums.FilterOption;
import finalproject.photocontest.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;

import static finalproject.photocontest.constants.GlobalConstants.ROLE_JUNKIE;
import static finalproject.photocontest.constants.GlobalConstants.ROLE_ORGANIZER;
import static finalproject.photocontest.models.enums.FilterOption.*;

@Controller
@RequestMapping("/dashboard")
public class DashboardJurorControllerMvc {

    private final LoginServiceMvc loginService;
    private final ContestService contestService;
    private final CategoryService categoryService;

    @Autowired
    public DashboardJurorControllerMvc(LoginServiceMvc loginService, ContestService contestService,
                                           CategoryService categoryService) {
        this.loginService = loginService;
        this.contestService = contestService;
        this.categoryService = categoryService;
    }

    @ModelAttribute("categories")
    public List<ContestCategory> fetchAllCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("phases")
    public ContestPhase[] fetchAllPhases() {
        return ContestPhase.values();
    }

    @GetMapping("/juror-contests")
    public String showJurorContests(Model model, HttpSession session) {
        try {
            var user = loginService.verifyAuthorizationJury(session, ROLE_ORGANIZER, ROLE_JUNKIE);
            var filter = new Filter().add(FilterOption.JUROR_ID, String.valueOf(user.getUserId()));

            model.addAttribute("user", user);
            model.addAttribute("form", new OrganizerContestFilterDto());
            model.addAttribute("jurorContests", contestService.getContestsAsJuror(filter));
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "dashboard-juror-contests";
    }

    @PostMapping("/juror-contests")
    public String showFilteredContests(Model model, HttpSession session,
                                       @Valid @ModelAttribute("form") OrganizerContestFilterDto form,
                                       BindingResult binding) {

        if (binding.hasErrors()) return "dashboard-juror-contests";

        try {
            var user = loginService.verifyAuthorizationJury(session, ROLE_ORGANIZER, ROLE_JUNKIE);

            var filter = new Filter().add(FilterOption.JUROR_ID, String.valueOf(user.getUserId()));
            if (form.getCategoryId() != null) filter.add(CATEGORY_ID, String.valueOf(form.getCategoryId()));
            if (form.getPhase() != null && !form.getPhase().isBlank())
                filter.add(PHASE, form.getPhase());
            if (form.getSearch() != null) filter.add(SEARCH, form.getSearch());
            if (form.getOpen() != null && form.getOpen()) filter.add(STATUS, "open");
            if (form.getOpen() != null && !form.getOpen()) filter.add(STATUS, "invitational");

            model.addAttribute("user", user);
            model.addAttribute("jurorContests", contestService.getContestsAsJuror(filter));

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "dashboard-juror-contests";
    }
}

