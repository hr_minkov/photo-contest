package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.dtos.dtosMvc.CreatePhotoDtoMvc;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.services.contracts.*;
import finalproject.photocontest.utils.ModelMapper;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/contests")
public class ContestJunkieControllerMvc {

    private final LoginServiceMvc loginService;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final PhotoResourceHandler photoResourceHandler;
    private final EvaluationService evaluationService;

    @Autowired
    public ContestJunkieControllerMvc(LoginServiceMvc loginService, ContestService contestService,
                                      PhotoService photoService, PhotoResourceHandler photoResourceHandler,
                                      EvaluationService evaluationService) {
        this.loginService = loginService;
        this.contestService = contestService;
        this.photoService = photoService;
        this.photoResourceHandler = photoResourceHandler;
        this.evaluationService = evaluationService;
    }

    @GetMapping("/{contestId}")
    public String showSingleContest(Model model, HttpSession session, @PathVariable int contestId) {

        try {
            var user = loginService.tryGetUser(session);
            var contest = contestService.getById(contestId);

            model.addAttribute("form", new CreatePhotoDtoMvc());
            model.addAttribute("user", user);
            model.addAttribute("contest", contest);

            if (contest.getPhase().equals(ContestPhase.FINISHED)) {
                model.addAttribute("evaluations",
                        evaluationService.getAllContestPhotoEvaluations(user.getUserId(), contestId));
            }

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "single-contest";
    }

    @PostMapping("/{contestId}/enroll")
    public String enroll(@PathVariable int contestId, HttpSession session, Model model) {
        try {
            var user = loginService.tryGetUser(session);
            contestService.enroll(contestId, user.getUserId());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "redirect:/contests/" + contestId;
    }

    @PostMapping("/{contestId}/leave")
    public String leave(@PathVariable int contestId, HttpSession session, Model model) {

        try {
            var user = loginService.tryGetUser(session);
            contestService.cancelEnrollment(contestId, user.getUserId());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/dashboard/open";
    }

    @PostMapping("/{contestId}/upload")
    public String uploadPhoto(Model model, HttpSession session, @PathVariable int contestId,
                              @Valid @ModelAttribute("form") CreatePhotoDtoMvc form, BindingResult binding) {

        var user = loginService.tryGetUser(session);
        var contest = contestService.getById(contestId);

        if (binding.hasErrors()) {
            model.addAttribute("form", form);
            model.addAttribute("user", user);
            model.addAttribute("contest", contest);
            return "single-contest";
        }

        try {
            var photo = new Photo(form.getTitle(), form.getStory(), user, contest);
            photoService.create(photo, form.getFile());
        } catch (Exception e) {
            binding.rejectValue("file", "file_error", "Illegal file type");
            model.addAttribute("form", form);
            model.addAttribute("user", user);
            model.addAttribute("contest", contest);
            return "single-contest";
        }
        model.addAttribute("contest", contestService.getById(contestId));
        return "redirect:/contests/" + contestId;
    }

    @PostMapping("/{contestId}/remove")
    public String removePhoto(Model model, HttpSession session, @PathVariable int contestId) {

        var user = loginService.tryGetUser(session);
        var contest = contestService.getById(contestId);

        if (photoResourceHandler.deleteMultipartPhotoIfPresent(user.getUserId(), contestId)) {
            model.addAttribute("contest", contestService.getById(contestId));
            return "redirect:/contests/" + contestId;
        }

        model.addAttribute("form", new CreatePhotoDtoMvc());
        model.addAttribute("user", user);
        model.addAttribute("contest", contest);
// visualize delete error?
        return "single-contest";
    }
}