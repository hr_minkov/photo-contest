package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.UpdateUserDto;
import finalproject.photocontest.models.dtos.dtosMvc.RegisterDto;
import finalproject.photocontest.services.LoginServiceMvcImpl;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.services.contracts.UserService;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static finalproject.photocontest.constants.GlobalConstants.ROLE_JUNKIE;
import static finalproject.photocontest.constants.GlobalConstants.ROLE_ORGANIZER;

@Controller
@RequestMapping("/users")
public class UserControllerMvc {

    private final UserService userService;
    private final ModelMapper mapper;
    private final LoginServiceMvcImpl loginServiceMvc;

    @Autowired
    public UserControllerMvc(UserService userService, ModelMapper mapper, LoginServiceMvcImpl loginServiceMvc) {

        this.userService = userService;
        this.mapper = mapper;
        this.loginServiceMvc = loginServiceMvc;
    }

    @GetMapping("/profile")
    public String showUserProfile(Model model, HttpSession session) {
        try {
            User user = loginServiceMvc.tryGetUser(session);
            model.addAttribute("user", user);

            return "single-user";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditProfile(@PathVariable int id, Model model, HttpSession session) {
        try {
            var user = loginServiceMvc.verifyAuthorization(session, ROLE_ORGANIZER, ROLE_JUNKIE);

            model.addAttribute("user", user);
            UpdateUserDto editDtoUser = new UpdateUserDto();
            editDtoUser.setId(user.getUserId());
            model.addAttribute("editProfileDto", editDtoUser);
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "edit-profile";
    }

    @PostMapping("/{id}/edit")
    public String updateUserProfile(@PathVariable int id,  Model model,
                                    @Valid @ModelAttribute("editProfileDto") UpdateUserDto userDto,
                                    BindingResult bindingResult) {

        userDto.setId(userDto.getId());

        if (bindingResult.hasErrors()) {
            model.addAttribute("editProfileDto", userDto);
            return "edit-profile";
        }

        var editedUser = mapper.fromDto(userDto, id);

        try {
            userService.update(editedUser);
            return "redirect:/winners";

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}
