package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.daos.PhotoEvaluationDao;
import finalproject.photocontest.models.daos.UserRankingInfoDao;
import finalproject.photocontest.models.dtos.dtosMvc.LoginDto;
import finalproject.photocontest.models.dtos.dtosMvc.RegisterDto;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.services.contracts.UserService;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping()
public class HomePageControllerMvc {

    private final UserService userService;
    private final EvaluationService evaluationService;
    private final ModelMapper mapper;

    @ModelAttribute("topPhotos")
    public List<PhotoEvaluationDao> fetchWinningPhotos() {
        return evaluationService.getTopPhotos(5);
    }

    @ModelAttribute("topRanked")
    public List<UserRankingInfoDao> fetchTopRankedJunkie() {
        return evaluationService.getTopUsers(9);
    }

    @ModelAttribute("negative")
    public List<Object> negativeCount() {
        var list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add(42);
        }
        return list;
    }

    @Autowired
    public HomePageControllerMvc(UserService userService, EvaluationService evaluationService, ModelMapper mapper) {
        this.userService = userService;
        this.evaluationService = evaluationService;
        this.mapper = mapper;
    }

    @GetMapping
    public String showHomePage(Model model) {
        return "index";
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerJunkie(Model model,
                                 @Valid @ModelAttribute("registerDto") RegisterDto registerDto,
                                 BindingResult bindingResult) {

        if (!registerDto.getConfirmPassword().equals(registerDto.getPassword()))
            bindingResult.rejectValue("confirmPassword",
                    "password_error",
                    "Passwords do not match");

        if (bindingResult.hasErrors())
            return "register";

        try {
            var user = mapper.fromDto(registerDto);
            userService.create(user);
            var loginDto = new LoginDto();
            loginDto.setUsername(user.getCredentials().getUsername());
            model.addAttribute("loginDto", loginDto);
            return "login";

        } catch (RuntimeException e) {
            bindingResult.rejectValue(
                    "username",
                    "username_error",
                    "Username already taken");
            return "register";
        }
    }
}
