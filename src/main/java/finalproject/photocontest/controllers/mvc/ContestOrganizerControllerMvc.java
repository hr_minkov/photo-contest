package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.ContestCategory;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.dtosMvc.CreateContestDtoMvc;
import finalproject.photocontest.services.contracts.*;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;
import java.util.Optional;

import static finalproject.photocontest.constants.GlobalConstants.ROLE_ORGANIZER;

@Controller
@RequestMapping("/contests")
public class ContestOrganizerControllerMvc {

    private final LoginServiceMvc loginService;
    private final ContestService contestService;
    private final CategoryService categoryService;
    private final UserService userService;
    private final ModelMapper mapper;

    @Autowired
    public ContestOrganizerControllerMvc(LoginServiceMvc loginService, ContestService contestService,
                                         CategoryService categoryService,
                                         UserService userService, ModelMapper mapper) {
        this.loginService = loginService;
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @ModelAttribute("categories")
    public List<ContestCategory> fetchAllCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("eligibleJurors")
    public List<User> eligibleForJuryUsers() {
        return userService.getAllJunkiesEligibleForJurorDuty();
    }

    @ModelAttribute("junkies")
    public List<User> fetchAllJunkies() {
        return userService.getAllActiveJunkies();
    }

    @GetMapping("/create")
    public String showContestCreateForm(Model model, HttpSession session) {

        try {
            User user = loginService.verifyAuthorization(session, ROLE_ORGANIZER);

            model.addAttribute("user", user);
            model.addAttribute("form", new CreateContestDtoMvc());
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "contest-organizer-create";
    }

    @PostMapping("/create")
    public String createContest(Model model, HttpSession session,
                                @Valid @ModelAttribute("form") CreateContestDtoMvc form, BindingResult binding) {

        var user = loginService.tryGetUser(session);

        if (binding.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("form", form);

            return "contest-organizer-create";
        }

        try {
            var contest = mapper.fromDto(form);
            contestService.create(contest);

        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("user", user);
            model.addAttribute("form", form);

            return "contest-organizer-create";

        }

        return "redirect:/dashboard/contests";
    }
}