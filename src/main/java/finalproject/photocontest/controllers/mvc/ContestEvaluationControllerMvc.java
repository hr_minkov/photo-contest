package finalproject.photocontest.controllers.mvc;

import finalproject.photocontest.models.dtos.PhotoReviewDto;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.services.contracts.LoginServiceMvc;
import finalproject.photocontest.services.contracts.PhotoService;
import finalproject.photocontest.utils.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/contests")
public class ContestEvaluationControllerMvc {

    private final LoginServiceMvc loginService;
    private final ContestService contestService;
    private final PhotoService photoService;
    private final ModelMapper mapper;
    private final EvaluationService evaluationService;

    @Autowired
    public ContestEvaluationControllerMvc(LoginServiceMvc loginService, ContestService contestService,
                                          PhotoService photoService, ModelMapper mapper,
                                          EvaluationService evaluationService) {
        this.loginService = loginService;
        this.contestService = contestService;
        this.photoService = photoService;
        this.mapper = mapper;
        this.evaluationService = evaluationService;
    }

    @GetMapping("/{contestId}/evaluate/{photoId}")
    public String showPhotoEvaluationForm(Model model, HttpSession session,
                                          @PathVariable int contestId, @PathVariable int photoId) {

        var user = loginService.tryGetUser(session);
        var photo = photoService.getById(photoId);

        //authorization -> JURORS FOR CONTESTS IN EVALUATION (Phase 2)

        model.addAttribute("user", user);
        model.addAttribute("photo", photo);
        model.addAttribute("form", new PhotoReviewDto());
        model.addAttribute("contest", contestService.getById(contestId));

        return "single-contest-evaluate";
    }

    @PostMapping("/{contestId}/evaluate/{photoId}")
    public String processPhotoEvaluationForm(Model model, HttpSession session,
                                             @PathVariable int contestId, @PathVariable int photoId,
                                             @Valid @ModelAttribute("form") PhotoReviewDto form,
                                             BindingResult binding) {

        var user = loginService.tryGetUser(session);
        var photo = photoService.getById(photoId);

        if (binding.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("photo", photo);
            model.addAttribute("form", form);
            model.addAttribute("contest", contestService.getById(contestId));

            return "single-contest-evaluate";
        }

        var review = mapper.fromDto(form, user.getUserId(), contestId, photoId);
        evaluationService.create(review);

        return "redirect:/contests/" + contestId;
    }
}