package finalproject.photocontest.utils;

import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.utils.contracts.Logger;
import finalproject.photocontest.utils.contracts.ScheduledTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasksImpl implements ScheduledTasks {

    private static final Logger logger = new ConsoleLogger();
//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private final ContestService contestService;
    private final EvaluationService evaluationService;

    @Autowired
    public ScheduledTasksImpl(ContestService contestService, EvaluationService evaluationService) {
        this.contestService = contestService;
        this.evaluationService = evaluationService;
    }

    @Scheduled(fixedRate = 5 * 60 * 1000)
    @Override
    public void kickOutUsersWithoutPhotos() {

        var contestsInEvaluationPhase = contestService.getAllInEvaluationPhase();
        contestsInEvaluationPhase.forEach(contest -> {
            evaluationService.removeParticipantsWhoDidNotSubmitPhoto(contest);
            logger.logInfo("Ensured all participants have photos for " + contest.getTitle());
        });
    }

    @Scheduled(fixedRate = 5 * 60 * 1000)
    @Override
    public void processFinishedContests() {

        var contestsNotProcessed = contestService.getAllNotProcessed();
        contestsNotProcessed.forEach(contest -> {
            evaluationService.addDefaultReviewsForPhotosNotEvaluatedByJurors(contest);
            evaluationService.processContestWinners(contest);
            evaluationService.updateUserRankingPoints(contest.getContestId());
            logger.logInfo("Processed scores for contest " + contest.getTitle());
        });
    }

}
