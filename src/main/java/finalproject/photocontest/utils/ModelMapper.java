package finalproject.photocontest.utils;

import finalproject.photocontest.models.*;
import finalproject.photocontest.models.daos.PhotoEvaluationDao;
import finalproject.photocontest.models.dtos.PhotoReviewDto;
import finalproject.photocontest.models.dtos.UpdateUserDto;
import finalproject.photocontest.models.daos.UserRankingInfoDao;
import finalproject.photocontest.models.dtos.dtosMvc.CreateContestDtoMvc;
import finalproject.photocontest.models.dtos.dtosMvc.RegisterDto;
import finalproject.photocontest.models.dtos.dtosRest.CreateContestDto;
import finalproject.photocontest.models.dtos.dtosRest.CreateUserDto;
import finalproject.photocontest.models.enums.FilterOption;
import finalproject.photocontest.services.contracts.*;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static finalproject.photocontest.constants.models.ModelsConstants.DEFAULT_CONTEST_COVER;
import static finalproject.photocontest.constants.models.ModelsConstants.DEFAULT_USER_AVATAR;

@Component
public class ModelMapper {

    private final CategoryService categoryService;
    private final UserService userService;
    private final UserRoleService userRoleService;
    private final PhotoService photoService;
    private final ContestValidationService contestValidationService;

    @Autowired
    public ModelMapper(CategoryService categoryService, UserService userService, UserRoleService userRoleService,
                       PhotoService photoService, ContestValidationService contestValidationService) {

        this.categoryService = categoryService;
        this.userService = userService;
        this.userRoleService = userRoleService;
        this.photoService = photoService;
        this.contestValidationService = contestValidationService;
    }

    //-----------------------------------------------------Users--------------------------------------------------------
    public User fromDto(CreateUserDto dto) {
        var credentials = new Credentials(dto.getUsername(), dto.getPassword());
        var user = new User(dto.getFirstName(), dto.getLastName(), credentials);
        user.setUserRole(userRoleService.getById(1));
        return user;
    }

    public User fromDto(RegisterDto dto) {
        var credentials = new Credentials(dto.getUsername(), dto.getPassword());
        var user = new User(dto.getFirstName(), dto.getLastName(), credentials);
        user.setUserRole(userRoleService.getById(1));
        user.setAvatar(DEFAULT_USER_AVATAR);
        return user;
    }

    public User fromDto(UpdateUserDto dto, int id) {
        var user = userService.getById(id);
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.getCredentials().setPassword(dto.getPassword());
        return user;
    }

    public UserRankingInfoDao compileRankingInfoFor(User user) {
        var currRank = user.getRank();
        int currPoints = user.getRankingPoints();

        return new UserRankingInfoDao(user, currRank.toString(),
                currRank.getPointsTillNextRank(currPoints), user.getRankingPoints(),
                userService.getPrizeCountForPlace(user.getUserId(), 1),
                userService.getPrizeCountForPlace(user.getUserId(), 2),
                userService.getPrizeCountForPlace(user.getUserId(), 3));
    }
    //-----------------------------------------------------Users--------------------------------------------------------

    //-----------------------------------------------------Contests------------------------------------------------------
    public Contest fromDto(CreateContestDto dto) {
        var contest = new Contest(dto.getTitle(), dto.getStartPhase1() ,
                dto.getStartPhase2(), dto.getEndPhase2(),
                categoryService.getById(dto.getCategoryId()), dto.isOpen());


        var jurors = new HashSet<>(userService.getAll(new Filter().add(FilterOption.ROLE, "organizer")));

        if (dto.getAddedJurors() != null) {
            for (int addedJurorId : dto.getAddedJurors()) {
                jurors.add(userService.getById(addedJurorId));
            }
        }

        contest.setJurors(jurors);
        return contest;
    }

    public Contest fromDto(CreateContestDtoMvc dto) {
        var contest = new Contest(dto.getTitle(), LocalDateTime.parse(dto.getStartPhase1()) ,
                LocalDateTime.parse(dto.getStartPhase2()), LocalDateTime.parse(dto.getEndPhase2()),
                categoryService.getById(dto.getCategoryId()), dto.isOpen());

        var jurors = new HashSet<>(userService.getAll(new Filter().add(FilterOption.ROLE, "organizer")));

        if (dto.getCoverPhotoLink() != null && !dto.getCoverPhotoLink().isBlank()) contest.setCover(dto.getCoverPhotoLink());
        else contest.setCover(DEFAULT_CONTEST_COVER);

        if (dto.getAddedJurors() != null) {
            for (int addedJurorId : dto.getAddedJurors()) {
                jurors.add(userService.getById(addedJurorId));
            }
        }

        contest.setJurors(jurors);
        return contest;
    }

    //-----------------------------------------------------Contests-----------------------------------------------------

    //------------------------------------------------------Photos------------------------------------------------------
    public PhotoEvaluationDao compilePhotoEvaluation(Photo photo, Set<Review> reviews, double scoreAvg, int rankingPlace) {
        return new PhotoEvaluationDao(photo, reviews, scoreAvg, rankingPlace);
    }

    public PhotoEvaluation fromDto(PhotoReviewDto dto, int jurorId, int contestId, int photoId) {
        var juror = userService.getById(jurorId);
        var photo = photoService.getByContestIdAndPhotoId(contestId, photoId, juror);

        contestValidationService.ensurePhotoReviewConsistent(dto);

        var review = dto.isValid()
                ? new Review(dto.getScore(), dto.getComment(), juror)
                : new Review(0, "Photo does not match contest category", juror);

        return new PhotoEvaluation(photo, review);
    }
    //------------------------------------------------------Photos------------------------------------------------------
}
