package finalproject.photocontest.utils;

import finalproject.photocontest.utils.contracts.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ConsoleLogger implements Logger {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ScheduledTasksImpl.class);

    @Override
    public void logInfo(String message) {
        logger.info(message);
    }

    @Override
    public void logWarning(String message) {
        logger.warn(message);
    }

    @Override
    public void logWarning(String message, Throwable e) {
        logger.warn(message, e);
    }
 }
