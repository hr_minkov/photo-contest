package finalproject.photocontest.utils;

import finalproject.photocontest.exceptions.IllegalRequestException;
import org.springframework.stereotype.Component;

@Component
public class QueryHelper {

    private QueryHelper() {
    }

    public static String getAsPartialSearchParam(String filterValue) {
        return '%' + filterValue + '%';
    }

    public static int getAsIntParam(String filterValue) {
        try {
            return Integer.parseInt(filterValue);
        } catch (NumberFormatException e) {
            throw new IllegalRequestException(filterValue + " is not a valid query parameter");
        }
    }
}
