package finalproject.photocontest.services;

import finalproject.photocontest.models.UserRole;
import finalproject.photocontest.repos.contracts.UserRoleRepository;
import finalproject.photocontest.services.contracts.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserRole getById(int id) {
        return userRoleRepository.getById(id);
    }

    @Override
    public UserRole getByName(String name) {
        return userRoleRepository.getByName(name);
    }

    @Override
    public List<UserRole> getAll() {
        return userRoleRepository.getAll();
    }
}
