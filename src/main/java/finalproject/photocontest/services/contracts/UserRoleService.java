package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.UserRole;

import java.util.List;

public interface UserRoleService {

    UserRole getById(int id);

    UserRole getByName(String name);

    List<UserRole> getAll();
}
