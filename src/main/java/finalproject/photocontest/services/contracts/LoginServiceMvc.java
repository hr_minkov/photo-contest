package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.User;

import javax.servlet.http.HttpSession;

public interface LoginServiceMvc {

    User authenticateUser(String username, String password);

    User tryGetUser(HttpSession session);

    User verifyAuthorization(HttpSession session, String... authorizedRoles);

    User verifyAuthorizationJury(HttpSession session, String... authorizedRoles);

}
