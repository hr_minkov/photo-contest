package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.Photo;
import finalproject.photocontest.models.User;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PhotoService {

    Photo getById(int id);

    Photo getByContestIdAndPhotoId(int contestId, int photoId, User issuer);

    List<Photo> getAll();

    List<Photo> getAll(Filter filter);

    Resource loadContestPhotoFileAsResource(int contestId, int photoId, User issuer);

    Photo create(Photo photo, MultipartFile file);
}
