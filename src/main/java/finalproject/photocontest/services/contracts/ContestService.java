package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;

import java.time.Duration;
import java.util.List;

public interface ContestService {

    Duration getTimeTillNextPhase(int id);

    Contest getById(int id);

    List<Contest> getAll();

    List<Contest> getAll(Filter filter);

    List<Contest> getContestsAvailableToUser(Filter filter);

    List<Contest> getContestsAsJuror(Filter filter);

    List<Contest> getAllNotProcessed();

    Contest create(Contest contest);

    Contest enroll(int contestId, int userId);

    Contest cancelEnrollment(int contestId, int userId);

    Contest delete(int contestId);

    Contest removePhoto(int contestId, int userId);

    Contest forceRemoveJunkieWithoutPhoto(int contestId, int userId);

    List<Contest> getAllInEvaluationPhase();

    Contest inviteUserToContest(int contestId, int userId);

}
