package finalproject.photocontest.services.contracts;

import finalproject.photocontest.models.ContestCategory;

import java.util.List;

public interface CategoryService {

    ContestCategory getById(int id);

    ContestCategory getByName(String name);

    List<ContestCategory> getAll();
}
