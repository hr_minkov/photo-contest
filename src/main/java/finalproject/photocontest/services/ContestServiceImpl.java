package finalproject.photocontest.services;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.ContestParticipant;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.enums.ContestPhase;
import finalproject.photocontest.models.enums.FilterOption;
import finalproject.photocontest.utils.QueryHelper;
import finalproject.photocontest.repos.contracts.ContestRepository;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.utils.contracts.PhotoResourceHandler;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import finalproject.photocontest.validation.contracts.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestRepository contestRepository;
    private final UserRepository userRepository;
    private final UserValidationService userValidationService;
    private final ContestValidationService contestValidationService;
    private final PhotoResourceHandler photoResourceHandler;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, UserRepository userRepository,
                              UserValidationService userValidationService,
                              ContestValidationService contestValidationService,
                              PhotoResourceHandler photoResourceHandler) {

        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
        this.userValidationService = userValidationService;
        this.contestValidationService = contestValidationService;
        this.photoResourceHandler = photoResourceHandler;
    }

    @Override
    public Duration getTimeTillNextPhase(int contestId) {

        var contest = contestRepository.getById(contestId);

        if (contest.getPhase() == ContestPhase.NOT_STARTED)
            return Duration.between(LocalDateTime.now(), contest.getStartPhase1());

        if (contest.getPhase() == ContestPhase.PHASE_I)
            return Duration.between(LocalDateTime.now(), contest.getStartPhase2());

        if (contest.getPhase() == ContestPhase.PHASE_II)
            Duration.between(LocalDateTime.now(), contest.getEndPhase2());

        return Duration.ZERO;
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    @Override
    public List<Contest> getAll(Filter filter) {
        return contestRepository.getAll(filter);
    }

    @Override
    public List<Contest> getContestsAvailableToUser(Filter filter) {
        return contestRepository.getAllAvailable(filter);
    }

    @Override
    public List<Contest> getAllNotProcessed() {
        return contestRepository.getAllNotProcessed();
    }

    @Override
    public List<Contest> getContestsAsJuror(Filter filter) {
        var user = userRepository.getById(QueryHelper.getAsIntParam(filter.get(FilterOption.JUROR_ID)));
        userValidationService.ensureIsEligibleForJuror(user);
        return contestRepository.getAll(filter);
    }

    @Override
    public Contest create(Contest contest) {

        contestValidationService
                .ensureTitleUnique(contest)
                .ensureScheduleTimelineConsistent(contest)
                .ensurePhaseDurationsConsistent(contest)
                .ensureJurorsEligible(contest);

        return contestRepository.create(contest);
    }

    @Override
    public Contest enroll(int contestId, int userId) {

        var user = userRepository.getById(userId);
        var contest = contestRepository.getById(contestId);

        userValidationService
                .ensureIsJunkie(user)
                .ensureIsNotJuror(user, contest);

        contestValidationService
                .ensureOpen(contest)
                .ensureInEnrollmentPhase(contest)
                .ensureNotAlreadyEnrolled(contest, user);

        var participant = new ContestParticipant(user, contest);

        return contestRepository.enroll(participant);
    }

    @Override
    public Contest cancelEnrollment(int contestId, int userId) {

        var contest = contestRepository.getById(contestId);
        var user = userRepository.getById(userId);

        contestValidationService
                .ensureInEnrollmentPhase(contest)
                .ensureEnrolled(contest, user);

        return userRepository.cancelEnrolment(contestId, userId);
    }

    @Override
    public Contest delete(int contestId) {
        var contest = contestRepository.getById(contestId);
        contestValidationService.ensureNotVisibleToUsers(contest);
        return contestRepository.delete(contest);
    }

    @Override
    public Contest removePhoto(int contestId, int userId) {

        var contest = contestRepository.getById(contestId);
        var user = userRepository.getById(userId);

        contestValidationService
                .ensureUserEnrolled(contest, user)
                .ensureInEnrollmentPhase(contest)
                .ensureUserUploadedPhoto(user, contest);

        if (!photoResourceHandler.deleteMultipartPhotoIfPresent(userId, contestId))
            throw new IllegalStateException("Photo could not be removed");

        return contest;
    }

    @Override
    public Contest forceRemoveJunkieWithoutPhoto(int contestId, int userId) {
        return userRepository.cancelEnrolment(contestId, userId);
    }

    @Override
    public List<Contest> getAllInEvaluationPhase() {
        return contestRepository.getAllInEvaluation();
    }

    @Override
    public Contest inviteUserToContest(int contestId, int userId) {
        User user = userRepository.getById(userId);
        Contest contest = contestRepository.getById(contestId);

        userValidationService
                .ensureIsJunkie(user)
                .ensureIsNotJuror(user, contest);

        contestValidationService
                .ensureInEnrollmentPhase(contest)
                .ensureNotAlreadyEnrolled(contest, user);

        var participant = new ContestParticipant(user, contest);

        return contestRepository.enroll(participant);
    }
}
