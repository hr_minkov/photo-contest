package finalproject.photocontest.services;

import finalproject.photocontest.models.*;
import finalproject.photocontest.models.daos.PhotoEvaluationDao;
import finalproject.photocontest.models.daos.UserRankingInfoDao;
import finalproject.photocontest.repos.contracts.ContestWinnerRepository;
import finalproject.photocontest.repos.contracts.PhotoObjectRepository;
import finalproject.photocontest.repos.contracts.PhotoReviewRepository;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.services.contracts.ContestService;
import finalproject.photocontest.services.contracts.EvaluationService;
import finalproject.photocontest.utils.ModelMapper;
import finalproject.photocontest.validation.contracts.ContestValidationService;
import finalproject.photocontest.validation.contracts.PhotoValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static finalproject.photocontest.constants.services.ServiceConstants.*;

@Service
public class EvaluationServiceImpl implements EvaluationService {

    private final ContestService contestService;
    private final ContestWinnerRepository contestWinnerRepository;
    private final UserRepository userRepository;
    private final PhotoObjectRepository photoObjectRepository;
    private final PhotoReviewRepository photoReviewRepository;
    private final PhotoValidationService photoValidationService;
    private final ContestValidationService contestValidationService;
    private final ModelMapper mapper;

    @Autowired
    public EvaluationServiceImpl(ContestService contestService, ContestWinnerRepository contestWinnerRepository,
                                 UserRepository userRepository, PhotoObjectRepository photoObjectRepository,
                                 PhotoReviewRepository photoReviewRepository, PhotoValidationService photoValidationService,
                                 ContestValidationService contestValidationService, ModelMapper mapper) {

        this.contestService = contestService;
        this.contestWinnerRepository = contestWinnerRepository;
        this.userRepository = userRepository;
        this.photoObjectRepository = photoObjectRepository;
        this.photoReviewRepository = photoReviewRepository;
        this.photoValidationService = photoValidationService;
        this.contestValidationService = contestValidationService;
        this.mapper = mapper;
    }

    @Override
    public PhotoEvaluationDao getSinglePhotoEvaluation(int userId, int contestId) {

        ensureUserEligibleToViewEvaluation(userId, contestId);

        var photo = photoObjectRepository.getByUserAndContestId(userId, contestId);
        var scoreAvg = getSingleContestPhotoScoreAvg(userId, contestId);
        var rankingPlace = getContestRankingPlacement(userId, contestId);

        return mapper.compilePhotoEvaluation(photo, photo.getReviews(), scoreAvg, rankingPlace);
    }

    @Override
    public List<PhotoEvaluationDao> getAllContestPhotoEvaluations(int userId, int contestId) {

        ensureUserEligibleToViewEvaluation(userId, contestId);

        List<PhotoEvaluationDao> evaluations = new ArrayList<>();

        var photos = contestService.getById(contestId).getPhotos();
        photos.forEach(photo -> {
            var photoEvaluationDto = new PhotoEvaluationDao();
            photoEvaluationDto.setPhoto(photo);
            photoEvaluationDto.setPhotoScoreAvg(photo.getReviews()
                    .stream()
                    .mapToDouble(Review::getScore).average()
                    .orElse(3));
            photoEvaluationDto.setReviews(photo.getReviews());
            evaluations.add(photoEvaluationDto);
        });
        evaluations.sort((a, b) -> Double.compare(b.getPhotoScoreAvg(), a.getPhotoScoreAvg()));
        // really ugly but complexity is much better this way, no time to improve... :(
        if (!evaluations.isEmpty()) {
            int place = 1;
            evaluations.get(0).setRankingPlace(place);
            for (int i = 1; i < evaluations.size(); i++) {
                double prevScore = evaluations.get(i-1).getPhotoScoreAvg();
                double currScore = evaluations.get(i).getPhotoScoreAvg();
                if (prevScore == currScore) {
                    evaluations.get(i).setRankingPlace(place);
                } else {
                    evaluations.get(i).setRankingPlace(++place);
                }
            }
        }
        return evaluations;
    }

    @Override
    public List<PhotoEvaluationDao> getTopPhotos(int count) {

        var evaluations = new ArrayList<PhotoEvaluationDao>();

        contestWinnerRepository.getTopRankedWinners(count)
                .forEach(winner -> evaluations.add(
                        mapper.compilePhotoEvaluation(
                                winner.getPhoto(),
                                winner.getPhoto().getReviews(),
                                winner.getPointsAvg(),
                                winner.getPlace())));
        return evaluations;
    }

    @Override
    public List<UserRankingInfoDao> getTopUsers(int count) {

        var topUsers = new ArrayList<UserRankingInfoDao>();

        userRepository.getTopRankedUsers(count)
                .forEach(user -> topUsers.add(
                        mapper.compileRankingInfoFor(user)));

        return topUsers;
    }

    @Override
    public PhotoEvaluation create(PhotoEvaluation photoEvaluation) {
        photoValidationService.ensureUnique(photoEvaluation);
        return photoReviewRepository.create(photoEvaluation);
    }

    @Override
    public void removeParticipantsWhoDidNotSubmitPhoto(Contest contest) {
        contestValidationService.ensurePastEnrollmentPhase(contest);
        forceRemoveJunkiesWithoutPhotosIfNeeded(contest);
    }

    @Override
    public void addDefaultReviewsForPhotosNotEvaluatedByJurors(Contest contest) {
        contest.getJurors().forEach(juror -> {
            var photosWithMissingReviews =
                    photoObjectRepository.getAllNotEvaluated(contest, juror.getUserId());
            photosWithMissingReviews.forEach(photo -> createDefaultReview(photo, juror));
        });
    }

    @Override
    public void updateUserRankingPoints(int contestId) {

        var contest = contestService.getById(contestId);
        contestValidationService.ensureFinished(contest);

        var pointsByUser = aggregatePointsForParticipation(contest);

        aggregatePointsForWinners(contest,
                pointsByUser,
                FIRST_PLACE,
                POINTS_FIRST_PLACE_SINGLE,
                POINTS_FIRST_PLACE_SHARED,
                POINTS_FIRST_PLACE_DOUBLED);

        aggregatePointsForWinners(contest,
                pointsByUser,
                SECOND_PLACE,
                POINTS_SECOND_PLACE_SINGLE,
                POINTS_SECOND_PLACE_SHARED,
                35);

        aggregatePointsForWinners(contest,
                pointsByUser,
                THIRD_PLACE,
                POINTS_THIRD_PLACE_SINGLE,
                POINTS_THIRD_PLACE_SHARED,
                20);

        pointsByUser.forEach((user, pointsEarned) -> {
            user.setRankingPoints(user.getRankingPoints() + pointsEarned);
            userRepository.update(user);
        });
    }

    @Override
    public void processContestWinners(Contest contest) {

        var scores = aggregatePhotoScoresAvg(contest);
        var avgScore = getScoresAvgAsList(scores);
        var rankingList = calculateContestPhotoRanking(scores);

        persistContestWinners(contest, avgScore, rankingList);
    }

    private void ensureUserEligibleToViewEvaluation(int userId, int contestId) {

        var user = userRepository.getById(userId);
        var contest = contestService.getById(contestId);

        contestValidationService
                .ensureFinished(contest);

        if (user.isOrganizer() || contest.getJurors().contains(user)) return;

        contestValidationService.ensureEnrolled(contest, user);
    }

    private double getSingleContestPhotoScoreAvg(int userId, int contestId) {
        return resolveSinglePhotoScore(userId, contestId);
    }

    private int getContestRankingPlacement(int userId, int contestId) {
        return resolveSinglePhotoRank(userId, contestId);
    }

    private void forceRemoveJunkiesWithoutPhotosIfNeeded(Contest contest) {

        var photos = contest.getPhotos();
        var participants = contest.getParticipants();

        if (photos.size() != participants.size()) {
            if (photos.size() > participants.size())
                throw new IllegalStateException(EVALUATION_PHOTOS_MORE_THAN_PARTICIPANTS_EX_MESSAGE);

            participants.stream()
                    .filter(participant -> photos.stream()
                            .noneMatch(photo -> photo.getAuthor().getUserId() == participant.getUserId()))
                    .forEach(participant -> contestService.forceRemoveJunkieWithoutPhoto(
                            contest.getContestId(), participant.getUserId()));
        }
    }

    private void createDefaultReview(Photo photo, User juror) {

        var defaultReview =
                new Review(3, EVALUATION_DEFAULT_COMMENT, juror);

        photoReviewRepository.create(new PhotoEvaluation(photo, defaultReview));
    }

    private double resolveSinglePhotoScore(int userId, int contestId) {
        return getAllReviews(userId, contestId).stream()
                .map(Review::getScore)
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0);
    }

    private int resolveSinglePhotoRank(int userId, int contestId) {

        var userScore = resolveSinglePhotoScore(userId, contestId);
        var contest = contestService.getById(contestId);
        var scoresList = getScoresAvgAsList(aggregatePhotoScoresAvg(contest));
        for (int i = 0; i < scoresList.size(); i++) {
            if (userScore == scoresList.get(i)) return i + 1;
        }

        throw new IllegalStateException(EVALUATION_USER_SCORE_NOT_IN_SCORES_LIST_EX_MESSAGE);
    }

    private List<Review> getAllReviews(int userId, int contestId) {
        ensureUserEligibleToViewEvaluation(userId, contestId);
        var photo = photoObjectRepository.getByUserAndContestId(userId, contestId);
        return new ArrayList<>(photo.getReviews());
    }

    private Map<Double, List<Photo>> aggregatePhotoScoresAvg(Contest contest) {

        Map<Double, List<Photo>> photoScoresAvg = new TreeMap<>(Comparator.reverseOrder());

        photoObjectRepository.getAll().stream()
                .filter(photo -> photo.getContest().getContestId() == contest.getContestId())
                .forEach(photo -> {
                    double score =
                            getSingleContestPhotoScoreAvg(photo.getAuthor().getUserId(), contest.getContestId());
                    photoScoresAvg.putIfAbsent(score, new ArrayList<>());
                    photoScoresAvg.get(score).add(photo);
                });

        return photoScoresAvg;
    }

    private ArrayList<Double> getScoresAvgAsList(Map<Double, List<Photo>> scores) {
        return new ArrayList<>(scores.keySet());
    }

    private void persistContestWinners(Contest contest, List<Double> avgScore, List<List<Photo>> rankingList) {

        if (rankingList.size() > 0)
            rankingList.get(0).forEach(winningPhoto ->
                    contestWinnerRepository.create(new ContestWinner(contest, winningPhoto, 1, avgScore.get(0))));

        if (rankingList.size() > 1)
            rankingList.get(1).forEach(winningPhoto ->
                    contestWinnerRepository.create(new ContestWinner(contest, winningPhoto, 2, avgScore.get(1))));

        if (rankingList.size() > 2)
            rankingList.get(2).forEach(winningPhoto ->
                    contestWinnerRepository.create(new ContestWinner(contest, winningPhoto, 3, avgScore.get(2))));
    }

    private List<List<Photo>> calculateContestPhotoRanking(Map<Double, List<Photo>> contestScores) {

        List<List<Photo>> ranking = new ArrayList<>();
        contestScores.forEach((score, photos) -> ranking.add(photos));

        return ranking;
    }

    private Map<User, Integer> aggregatePointsForParticipation(Contest contest) {
        Map<User, Integer> pointsForParticipation = new HashMap<>();

        contestService.getById(contest.getContestId()).getParticipants()
                .forEach(participant -> {
                    if (contest.isOpen()) pointsForParticipation.put(participant, 1);
                    else pointsForParticipation.put(participant, 3);
                });

        return pointsForParticipation;
    }

    private void aggregatePointsForWinners(Contest contest, Map<User, Integer> pointsByUser,
                                           int placeFinished, int ifSingle, int ifShared, int ifDoublePoints) {

        List<ContestWinner> winners =
                contestWinnerRepository.getByPlaceFinished(contest.getContestId(), placeFinished);

        if (winners.isEmpty()) return;

        if (winners.size() == 1)
            addPointsForSinglePlace(contest, pointsByUser, winners, placeFinished, ifSingle, ifDoublePoints);
        else
            addPointsForSharedPlaces(pointsByUser, ifShared, winners);
    }

    private void addPointsForSinglePlace(Contest contest, Map<User, Integer> pointsByUser, List<ContestWinner> winners,
                                         int placeFinished, int ifSingle, int ifDoublePoints) {

        double contestWinnerPoints = winners.get(0).getPointsAvg();
        double contestBestSecondPlace =
                contestWinnerRepository.getByPlaceFinished(contest.getContestId(), 2).get(0).getPointsAvg();

        var user = winners.get(0).getPhoto().getAuthor();
        if (placeFinished == 1 && contestBestSecondPlace * 2 <= contestWinnerPoints)
            pointsByUser.replace(user, pointsByUser.get(user) + ifDoublePoints);
        else
            pointsByUser.replace(user, pointsByUser.get(user) + ifSingle);
    }

    private void addPointsForSharedPlaces(Map<User, Integer> pointsByUser, int ifShared, List<ContestWinner> winners) {
        winners.forEach(winner -> {
            var user = (winner.getPhoto().getAuthor());
            pointsByUser.replace(user, pointsByUser.get(user) + ifShared);
        });
    }
}
