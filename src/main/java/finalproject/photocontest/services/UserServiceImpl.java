package finalproject.photocontest.services;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.exceptions.IllegalRequestException;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.enums.FilterOption;
import finalproject.photocontest.models.enums.UserRank;
import finalproject.photocontest.repos.contracts.UserRepository;
import finalproject.photocontest.services.contracts.UserService;
import finalproject.photocontest.validation.contracts.UserValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static finalproject.photocontest.constants.GlobalConstants.ROLE_JUNKIE;
import static finalproject.photocontest.constants.services.ServiceConstants.USER_ALREADY_HAS_ACTIVE_STATUS_EX_MESSAGE;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserValidationService userValidationService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserValidationService userValidationService) {
        this.userRepository = userRepository;
        this.userValidationService = userValidationService;
    }

    @Override
    public int getPrizeCountForPlace(int userId, int placeFinished) {
        return userRepository.getContestAwardsCountFor(userId, placeFinished);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getActiveByUsername(username);
    }

    @Override
    public User getByCredentials(String username, String password) {
        return userRepository.getActiveByCredentials(username, password);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<User> getAll(Filter filter) {
        return userRepository.getAll(filter);
    }

    @Override
    public List<User> getAllJunkiesEligibleForJurorDuty() {

        Filter filter = new Filter()
                .add(FilterOption.JUROR_ID, ROLE_JUNKIE)
                .add(FilterOption.MIN_RANKING_POINTS, String.valueOf(UserRank.MASTER.getRankMinPoints()));

        return userRepository.getAll(filter);
    }

    @Override
    public User create(User user) {
        userValidationService.ensureUsernameUnique(user);
        return userRepository.create(user);
    }

    @Override
    public User update(User user) {
        userValidationService.ensureUsernameUnique(user);
        return userRepository.update(user);
    }

    @Override
    public User suspend(User issuer, int userToSuspendId) {

        var userToSuspend = userRepository.getById(userToSuspendId);
        userValidationService
                .ensurePermittedToAlterUserActiveStatus(issuer)
                .ensureIsJunkie(userToSuspend);

        return userRepository.delete(userToSuspend);
    }

    @Override
    public User restore(User issuer, int userToRestoreId) {
        try {
            var userToRestore = userRepository.getInactiveById(userToRestoreId);
            userValidationService
                    .ensurePermittedToAlterUserActiveStatus(issuer);
            return userRepository.restore(userToRestore);

        } catch (EntityNotFoundException e) {
            userRepository.getById(userToRestoreId);
            throw new IllegalRequestException(USER_ALREADY_HAS_ACTIVE_STATUS_EX_MESSAGE);
        }
    }

    @Override
    public User delete(User user) {
        return userRepository.delete(user);
    }

    @Override
    public List<User> getAllActiveJunkies() {
        return userRepository.getAllActiveJunkies();
    }

    @Override
    public List<User> getAllActiveJunkiesNotParticipatingAlready(int contestId) {
        return userRepository.getAllJunkiesEligibleToInvite(contestId);
    }
}
