package finalproject.photocontest.repos;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.UserRole;
import finalproject.photocontest.repos.contracts.UserRoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRoleRepositorySql implements UserRoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRoleRepositorySql(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserRole getById(int id) {

        try (Session session = sessionFactory.openSession()) {
            var role = session.get(UserRole.class, id);
            if (role == null)  throw new EntityNotFoundException("Role", id);

            return role;
        }
    }

    @Override
    public UserRole getByName(String name) {

        try (Session session = sessionFactory.openSession()) {

            var query = session.createQuery("from UserRole where name = :name", UserRole.class);
            query.setParameter("name", name);

            var result = query.getResultList();
            if (result.size() == 0)  throw new EntityNotFoundException("Role", "name", name);

            return result.get(0);
        }
    }

    @Override
    public List<UserRole> getAll() {

        try (Session session = sessionFactory.openSession()) {
            Query<UserRole> query = session.createQuery("from UserRole", UserRole.class);

            return query.getResultList();
        }
    }
}
