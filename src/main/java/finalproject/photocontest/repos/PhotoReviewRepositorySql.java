package finalproject.photocontest.repos;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.PhotoEvaluation;
import finalproject.photocontest.repos.contracts.PhotoReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PhotoReviewRepositorySql implements PhotoReviewRepository {

    private final SessionFactory sessionFactory;

    public PhotoReviewRepositorySql(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PhotoEvaluation getPhotoReviewByJurorAndPhotoId(int jurorId, int photoId) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from PhotoEvaluation pr where " +
                    "pr.photo.id = :photoId and pr.review.juror.id = :jurorId", PhotoEvaluation.class);

            query.setParameter("photoId", photoId);
            query.setParameter("jurorId", jurorId);

            var result = query.getResultList();
            if (result.isEmpty()) throw new EntityNotFoundException();

            return result.get(0);
        }
    }

    @Transactional
    @Override
    public PhotoEvaluation create(PhotoEvaluation photoEvaluation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(photoEvaluation.getReview());
            session.save(photoEvaluation);
            session.getTransaction().commit();
            return photoEvaluation;
        }
    }
}
