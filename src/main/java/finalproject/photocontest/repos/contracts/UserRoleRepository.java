package finalproject.photocontest.repos.contracts;

import finalproject.photocontest.models.UserRole;

public interface UserRoleRepository extends ReadOnlyRepository<UserRole>{
    UserRole getByName(String name);
}
