package finalproject.photocontest.repos.contracts;


import java.util.List;

public interface ReadOnlyRepository<T> {

    List<T> getAll();

    T getById(int id);
}
