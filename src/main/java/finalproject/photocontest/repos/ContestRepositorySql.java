package finalproject.photocontest.repos;

import finalproject.photocontest.exceptions.EntityNotFoundException;
import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.ContestParticipant;
import finalproject.photocontest.models.Filter;
import finalproject.photocontest.repos.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static finalproject.photocontest.utils.QueryHelper.*;
import static finalproject.photocontest.models.enums.FilterOption.*;

@Repository
public class ContestRepositorySql implements ContestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositorySql(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Contest getById(int id) {

        try (Session session = sessionFactory.openSession()) {
            var contest = session.get(Contest.class, id);
            if (contest == null) throw new EntityNotFoundException("Contest", id);

            return contest;
        }
    }

    @Override
    public Contest getByTitle(String title) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Contest where title = :title",
                    Contest.class);
            query.setParameter("title", title);
            var result = query.getResultList();

            if (result.isEmpty()) throw new EntityNotFoundException();

            return result.get(0);
        }
    }

    @Override
    public List<Contest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Contest order by startPhase1 desc ",
                    Contest.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Contest> getAll(Filter filter) {
        try (Session session = sessionFactory.openSession()) {
            var query = buildConditionalContestQuery(filter, session);
            return query.getResultList();
        }
    }

    @Override
    public List<Contest> getAllAvailable(Filter filter) {
        try (Session session = sessionFactory.openSession()) {
            var query = buildAvailableContestsQuery(filter, session);
            return query.getResultList();
        }
    }

    @Override
    public List<Contest> getAllNotProcessed() {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from Contest c where  :now >= c.endPhase2 and  " +
                    "c not in (select distinct cw.contest from ContestWinner cw)",
                    Contest.class);

            query.setParameter("now", LocalDateTime.now());
            return query.getResultList();
        }
    }

    @Override
    public List<Contest> getAllInEvaluation() {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from Contest c where  :now >= c.startPhase2 and :now <= c.endPhase2",
                    Contest.class);

            query.setParameter("now", LocalDateTime.now());
            return query.getResultList();
        }
    }

    @Transactional
    @Override
    public Contest create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(contest);
            session.getTransaction().commit();
            return contest;
        }
    }

    @Transactional
    @Override
    public Contest enroll(ContestParticipant participant) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(participant);
            session.getTransaction().commit();
            return participant.getContest();
        }
    }

    @Transactional
    @Override
    public Contest delete(Contest contest) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(contest);
            session.getTransaction().commit();
            return contest;
        }
    }

    private Query<Contest> buildConditionalContestQuery(Filter filter, Session session) {

        StringBuilder queryBuilder = new StringBuilder("from Contest c ");
        List<String> additions = new ArrayList<>();

        if (filter.containsOption(USER_ID))
            additions.add(" :userId in (select participant.userId from ContestParticipant where contest = c) ");

        if (filter.containsOption(JUROR_ID))
            additions.add(" :jurorId in (select juror.userId from ContestJuror where contest = c) ");

        if (filter.containsOption(CATEGORY_ID))
            additions.add(" c.category.categoryId = :categoryId ");

        if (filter.containsOption(SEARCH))
            additions.add(" (c.title like :search or c.category.name like :search) ");

        if (filter.containsOption(PHASE)) {
            if (filter.get(PHASE).equalsIgnoreCase("Not Started"))
                additions.add(" :now < c.startPhase1 ");
            if (filter.get(PHASE).equalsIgnoreCase("Enrollment"))
                additions.add(" :now >= c.startPhase1 and :now < c.startPhase2 ");
            if (filter.get(PHASE).equalsIgnoreCase("Evaluation"))
                additions.add(" :now >= c.startPhase2 and :now < c.endPhase2 ");
            if (filter.get(PHASE).equalsIgnoreCase("Finished"))
                additions.add(" :now >= c.endPhase2 ");
        }

        if (filter.containsOption(STATUS)) {
            if (filter.get(STATUS).equalsIgnoreCase("open"))
                additions.add(" c.isOpen = true ");
            if (filter.get(STATUS).equalsIgnoreCase("invitational"))
                additions.add(" c.isOpen = false ");
        }

        if (!additions.isEmpty())
            queryBuilder.append(" where ")
                    .append(String.join(" and ", additions));

        queryBuilder.append(" order by c.startPhase1 desc");

        var query = session.createQuery(queryBuilder.toString(), Contest.class);

        if (filter.containsOption(USER_ID))
            query.setParameter("userId", getAsIntParam(filter.get(USER_ID)));

        if (filter.containsOption(JUROR_ID))
            query.setParameter("jurorId", getAsIntParam(filter.get(JUROR_ID)));

        if (filter.containsOption(CATEGORY_ID))
            query.setParameter("categoryId", getAsIntParam(filter.get(CATEGORY_ID)));

        if (filter.containsOption(PHASE))
            query.setParameter("now", LocalDateTime.now());

        if (filter.containsOption(SEARCH))
            query.setParameter("search", getAsPartialSearchParam(filter.get(SEARCH)));

        return query;
    }

    private Query<Contest> buildAvailableContestsQuery(Filter filter, Session session) {

        StringBuilder queryBuilder = new StringBuilder("from Contest c where " +
                " c.isOpen=true " +
                " and :now >= c.startPhase1 and :now < c.startPhase2 " +
                " and :userId not in (select cp.participant.userId from ContestParticipant cp " +
                " where cp.contest.contestId = c.contestId) " +
                " and :userId not in (select cj.juror.userId from ContestJuror cj " +
                " where cj.contest.contestId = c.contestId) ");

        List<String> additions = new ArrayList<>();

        if (filter.containsOption(CATEGORY_ID))
            additions.add(" c.category.categoryId = :categoryId ");

        if (filter.containsOption(SEARCH))
            additions.add(" (c.title like :search or c.category.name like :search) ");

        if (!additions.isEmpty())
            queryBuilder.append(" and ")
                    .append(String.join(" and ", additions));

        var query = session.createQuery(queryBuilder.toString(), Contest.class);

        query.setParameter("now", LocalDateTime.now());
        query.setParameter("userId", filter.get(USER_ID));

        if (filter.containsOption(CATEGORY_ID))
            query.setParameter("categoryId", getAsIntParam(filter.get(CATEGORY_ID)));

        if (filter.containsOption(SEARCH))
            query.setParameter("search", getAsPartialSearchParam(filter.get(SEARCH)));

        return query;
    }
}
