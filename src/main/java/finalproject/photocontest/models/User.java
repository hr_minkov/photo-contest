package finalproject.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import finalproject.photocontest.models.enums.UserRank;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

import static finalproject.photocontest.constants.GlobalConstants.ROLE_JUNKIE;
import static finalproject.photocontest.constants.GlobalConstants.ROLE_ORGANIZER;

@Entity
@Table(name = "users_details")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "ranking_points")
    private int rankingPoints;

    @Column(name = "active")
    private boolean active;

    @Column(name = "avatar_link")
    private String avatar;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private Credentials credentials;

    @OneToOne
    @JoinColumn(name = "role_id")
    private UserRole userRole;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contests_jurors",
            joinColumns = @JoinColumn(name = "juror_id"),
            inverseJoinColumns = @JoinColumn(name = "contest_id")
    )
    private Set<Contest> contestsAsJury;

    public User() {
    }

    public User(String firstName, String lastName, Credentials credentials) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.credentials = credentials;
        this.active = true;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getRankingPoints() {
        return rankingPoints;
    }

    public void setRankingPoints(int scores) {
        this.rankingPoints = scores;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Set<Contest> getContestsAsJury() {
        return contestsAsJury;
    }

    public void setContestsAsJury(Set<Contest> contestsAsJury) {
        this.contestsAsJury = contestsAsJury;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public UserRank getRank() {
        return UserRank.getRankFor(this.rankingPoints);
    }

    @JsonIgnore
    public boolean isJunkie() {
        return userRole.getName().equalsIgnoreCase(ROLE_JUNKIE);
    }

    @JsonIgnore
    public boolean isOrganizer() {
        return userRole.getName().equalsIgnoreCase(ROLE_ORGANIZER);
    }

    @JsonIgnore
    public boolean isEligibleForJuror() {
        return isOrganizer() || getRank().ordinal() >= UserRank.MASTER.ordinal();
    }

    @JsonIgnore
    public boolean isActive() {
        return active;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(credentials, user.credentials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(credentials);
    }
}
