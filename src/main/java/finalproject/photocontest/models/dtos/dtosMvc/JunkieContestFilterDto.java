package finalproject.photocontest.models.dtos.dtosMvc;

public class JunkieContestFilterDto {

    private String search;
    private Integer categoryId;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
