package finalproject.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "photos_reviews")
public class PhotoEvaluation {

    @Id
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @OneToOne
    @JoinColumn(name = "review_id")
    private Review review;

    public PhotoEvaluation() {
    }

    public PhotoEvaluation(Photo photo, Review review) {
        this.photo = photo;
        this.review = review;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhotoEvaluation that = (PhotoEvaluation) o;
        return Objects.equals(photo, that.photo) && Objects.equals(review, that.review);
    }

    @Override
    public int hashCode() {
        return Objects.hash(photo, review);
    }
}
