package finalproject.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contests_participants")
public class ContestParticipant {

    @Id
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User participant;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    public ContestParticipant() {
    }

    public ContestParticipant(User participant, Contest contest) {
        this.participant = participant;
        this.contest = contest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getParticipant() {
        return participant;
    }

    public void setParticipant(User user) {
        this.participant = user;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestParticipant that = (ContestParticipant) o;
        return Objects.equals(participant, that.participant) && Objects.equals(contest, that.contest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(participant, contest);
    }
}
