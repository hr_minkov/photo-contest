package finalproject.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "contests_winners")
@Entity
public class ContestWinner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @OneToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @Column(name = "place")
    private int place;

    @Column(name = "points")
    private double pointsAvg;

    public ContestWinner() {
    }

    public ContestWinner(Contest contest, Photo photo, int place, double pointsAvg) {
        this.contest = contest;
        this.photo = photo;
        this.place = place;
        this.pointsAvg = pointsAvg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contestParticipated) {
        this.contest = contestParticipated;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photoParticipated) {
        this.photo = photoParticipated;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int placeOfRanking) {
        this.place = placeOfRanking;
    }

    public double getPointsAvg() {
        return pointsAvg;
    }

    public void setPointsAvg(double pointsWon) {
        this.pointsAvg = pointsWon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestWinner that = (ContestWinner) o;
        return Objects.equals(contest, that.contest) && Objects.equals(photo, that.photo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contest, photo);
    }
}
