package finalproject.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int photoId;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User author;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @Column(name = "type")
    private String extension;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "photos_reviews",
            joinColumns = @JoinColumn(name = "photo_id"),
            inverseJoinColumns = @JoinColumn(name = "review_id")
    )
    private Set<Review> reviews;

    @JsonIgnore
    public boolean isEvaluatedBy(User user) {
        return reviews.stream().anyMatch(review -> review.getJuror().equals(user));
    }

    @JsonIgnore
    public Double getRatingFor(User user) {
        return reviews.stream().
                filter(review -> review.getJuror().equals(user))
                .map(Review::getScore)
                .map(Integer::doubleValue).findAny()
                .orElseThrow(() -> new IllegalStateException("User has not evaluated this photo"));
    }

    public Photo() {
    }

    public Photo(String title, String story, User author, Contest contest) {
        this.title = title;
        this.story = story;
        this.author = author;
        this.contest = contest;
    }

    public int getPhotoId() {
        return photoId;
    }

    public Photo setPhotoId(int photoId) {
        this.photoId = photoId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Photo setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getStory() {
        return story;
    }

    public Photo setStory(String story) {
        this.story = story;
        return this;
    }

    public User getAuthor() {
        return author;
    }

    public Photo setAuthor(User owner) {
        this.author = owner;
        return this;
    }

    public Contest getContest() {
        return contest;
    }

    public Photo setContest(Contest contestParticipated) {
        this.contest = contestParticipated;
        return this;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String type) {
        this.extension = type;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public Photo setReviews(Set<Review> reviews) {
        this.reviews = reviews;
        return this;
    }

    public String getAssociatedFileName() {
        return contest.getContestId() + "." + photoId + extension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return Objects.equals(title, photo.title)
                && Objects.equals(story, photo.story)
                && Objects.equals(author, photo.author)
                && Objects.equals(contest, photo.contest)
                && Objects.equals(extension, photo.extension);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, story, author, contest, extension);
    }
}
