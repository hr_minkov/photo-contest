package finalproject.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import finalproject.photocontest.models.enums.ContestPhase;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "title")
    private String title;

    @Column(name = "start_ph1")
    private LocalDateTime startPhase1;

    @Column(name = "start_ph2")
    private LocalDateTime startPhase2;

    @Column(name = "end_ph2")
    private LocalDateTime endPhase2;

    @OneToOne
    @JoinColumn(name = "category_id")
    private ContestCategory category;

    @Column(name = "cover_link")
    private String cover;

    @Column(name = "is_open")
    private boolean isOpen;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<Photo> photos;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contests_participants",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> participants;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contests_jurors",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "juror_id")
    )
    private Set<User> jurors;

    public Contest() {
    }

    public int getContestId() {
        return contestId;
    }

    public Contest(String title, LocalDateTime startPhase1, LocalDateTime startPhase2, LocalDateTime endPhase2,
                   ContestCategory category, boolean isOpen) {
        this.title = title;
        this.startPhase1 = startPhase1;
        this.startPhase2 = startPhase2;
        this.endPhase2 = endPhase2;
        this.category = category;
        this.isOpen = isOpen;
    }


    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartPhase1() {
        return startPhase1;
    }

    public void setStartPhase1(LocalDateTime startPhase1) {
        this.startPhase1 = startPhase1;
    }

    public LocalDateTime getStartPhase2() {
        return startPhase2;
    }

    public void setStartPhase2(LocalDateTime startPhase2) {
        this.startPhase2 = startPhase2;
    }

    public LocalDateTime getEndPhase2() {
        return endPhase2;
    }

    public void setEndPhase2(LocalDateTime endPhase2) {
        this.endPhase2 = endPhase2;
    }

    public ContestCategory getCategory() {
        return category;
    }

    public void setCategory(ContestCategory category) {
        this.category = category;
    }


    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public ContestPhase getPhase() {
        if (LocalDateTime.now().isBefore(startPhase1)) return ContestPhase.NOT_STARTED;
        if (LocalDateTime.now().isBefore(startPhase2)) return ContestPhase.PHASE_I;
        if (LocalDateTime.now().isBefore(endPhase2)) return ContestPhase.PHASE_II;
        return ContestPhase.FINISHED;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public Set<User> getJurors() {
        return jurors;
    }

    public void setJurors(Set<User> jurors) {
        this.jurors = jurors;
    }

    @JsonIgnore
    public boolean containsUserPhoto(User user) {
        return photos.stream().anyMatch(photo -> photo.getAuthor().equals(user));
    }

    @JsonIgnore
    public Photo getPhotoFor(User user) {
        return photos.stream()
                .filter(photo -> photo.getAuthor().equals(user)).findAny()
                .orElseThrow(() -> new IllegalStateException("User has not uploaded photo "));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return Objects.equals(title, contest.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
