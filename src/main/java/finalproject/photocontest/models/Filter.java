package finalproject.photocontest.models;

import finalproject.photocontest.models.enums.FilterOption;

import java.util.HashMap;
import java.util.Map;

public class Filter {

    private final Map<FilterOption, String> filters;

    public Filter() {
        this.filters = new HashMap<>();
    }

    public boolean containsOption(FilterOption key) {
        return filters.containsKey(key);
    }

    public String get(FilterOption key) {
        return filters.get(key);
    }

    public Filter add(FilterOption key, String filterValue) {
        filters.put(key, filterValue);
        return this;
    }
}
