package finalproject.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contests_jurors")
public class ContestJuror {

    @Id
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "juror_id")
    private User juror;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    public ContestJuror() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getJuror() {
        return juror;
    }

    public void setJuror(User juror) {
        this.juror = juror;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestJuror that = (ContestJuror) o;
        return Objects.equals(juror, that.juror) && Objects.equals(contest, that.contest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(juror, contest);
    }
}
