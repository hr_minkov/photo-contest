package finalproject.photocontest.models.enums;

public enum FilterOption {
    CONTEST_ID,
    AUTHOR_ID,
    SEARCH,
    JUROR_ID,
    STATUS,
    PHASE,
    CATEGORY_ID,
    USER_ID,
    ROLE,
    MIN_RANKING_POINTS
}
