package finalproject.photocontest.cloud;

import com.google.api.services.storage.model.StorageObject;
import com.google.common.io.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;


@RestController
@RequestMapping(value = "/cloud")
public class Controller {

    private final GoogleStorageClientAdapter googleStorageClientAdapter;

    @Autowired
    public Controller(GoogleStorageClientAdapter googleStorageClientAdapter) {
        this.googleStorageClientAdapter = googleStorageClientAdapter;
    }

    @PostMapping(path = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public Boolean uploadFile(@RequestPart(value = "file", required = true) MultipartFile files) {
        try {
            return googleStorageClientAdapter.upload(files, "prefix");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @GetMapping(name = "file-download", path = "download")
    public ResponseEntity<ByteArrayResource> fileDownload(@RequestParam(value = "file", required = true) String path
    ) {
        try {
            StorageObject object = googleStorageClientAdapter.download(path);

            byte[] res = Files.toByteArray((File) object.get("file"));
            ByteArrayResource resource = new ByteArrayResource(res);

//            var file= ((File) object.get("file"));
//            var result = Paths.get(file.getPath().substring(2));
//            java.nio.file.Files.delete(result);

            return ResponseEntity.ok()
                    .contentLength(res.length)
                    .header("Content-type", "application/octet-stream")
                    .header("Content-disposition", "attachment; filename=\"" + path + "\"").body(resource);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("No such file or directory");
        }
    }

    @DeleteMapping(path = "/delete")
    public Boolean delete(@RequestParam(value = "name", required = true) String fileName) {
        try {
            googleStorageClientAdapter.delete(fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    @GetMapping("/link")
    public String getByName(@RequestParam(value = "name", required = true) String fileName) {
        try {
            return googleStorageClientAdapter.getByName(fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return "kur";
        }
    }
}