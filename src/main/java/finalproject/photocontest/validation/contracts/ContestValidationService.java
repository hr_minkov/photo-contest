package finalproject.photocontest.validation.contracts;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.User;
import finalproject.photocontest.models.dtos.PhotoReviewDto;

public interface ContestValidationService {

    ContestValidationService ensureUserEnrolled(Contest contest, User user);

    ContestValidationService ensurePastEnrollmentPhase(Contest contest);

    ContestValidationService ensureInEnrollmentPhase(Contest contest);

    ContestValidationService ensureFinished(Contest contest);

    ContestValidationService ensureNotFinished(Contest contest);

    ContestValidationService ensureOpen(Contest contest);

    ContestValidationService ensureTitleUnique(Contest contest);

    ContestValidationService ensureScheduleTimelineConsistent(Contest contest);

    ContestValidationService ensurePhaseDurationsConsistent(Contest contest);

    ContestValidationService ensureJurorsEligible(Contest contest);

    ContestValidationService ensureNotAlreadyEnrolled(Contest contest, User user);

    ContestValidationService ensureEnrolled(Contest contest, User user);

    ContestValidationService ensureNotVisibleToUsers(Contest contest);

    ContestValidationService ensurePermissionsToViewContestPhotos(int userId, int contestId);

    ContestValidationService ensurePhotoPresent(int contestId, int photoId);

    ContestValidationService ensureUserHasNoPhotoUploaded(User user, Contest contest);

    ContestValidationService ensureUserUploadedPhoto(User user, Contest contest);

    ContestValidationService ensurePermissionsToViewContest(int contestId, int userId);

    ContestValidationService ensurePhotoReviewConsistent(PhotoReviewDto dto);
}
