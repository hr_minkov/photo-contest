package finalproject.photocontest.validation.contracts;

import finalproject.photocontest.models.Contest;
import finalproject.photocontest.models.User;

public interface UserValidationService {

    UserValidationService ensureUsernameUnique(User user);

    UserValidationService ensureIsJunkie(User user);

    UserValidationService ensurePermittedToAlterUserActiveStatus(User user);

    UserValidationService ensureIsEligibleForJuror(User user);

    UserValidationService ensureIsNotJuror(User user, Contest contest);
}
